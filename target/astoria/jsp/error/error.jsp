<%@ page isErrorPage="true" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<html>
<head>
<title>Error Page</title>
</head>
<body>
	<jsp:include page="/jsp/header/header.jsp"></jsp:include>
	<jsp:include page="/jsp/menu/menu.jsp"></jsp:include>

	<div class="container-fluid">
		<div class="row">
			<div class="col-sm-12 col-md-12 col-lg-12">
				<img
					src="${pageContext.servletContext.contextPath}/images/error/error500.png"
					class="img-responsive" alt="error500">
			</div>
			<div class="col-sm-12 col-md-12 col-lg-12">
				<h3>Request from ${pageContext.errorData.requestURI} is failed</h3>
			</div>
			<div class="col-sm-12 col-md-12 col-lg-12">
				<h3>Servlet name or type: ${pageContext.errorData.servletName}</h3>
			</div>
			<div class="col-sm-12 col-md-12 col-lg-12">
				<h3>Status code: ${pageContext.errorData.statusCode}</h3>
			</div>
			<div class="col-sm-12 col-md-12 col-lg-12">
				<h3>Exception: ${pageContext.errorData.throwable}</h3>
			</div>
		</div>
	</div>




	<div class="panel panel-warning">
		<div class="panel-footer"><jsp:include
				page="/jsp/header/footer.jsp" /></div>
	</div>
</body>
</html>
