<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>


<c:if test="${user.admin}">
	<jsp:include page="/jsp/crud/booking/adminbookinglist.jsp"></jsp:include>
</c:if>
<c:if test="${not empty user and not user.admin}">
	<jsp:include page="/jsp/crud/booking/userbookinglist.jsp"></jsp:include>
</c:if>