<form>
	<select name="language" onchange="submit()">
		<option value=""></option>
		<option value="en" ${language == 'en' ? 'selected' : ''}>English</option>
        <option value="ru" ${language == 'ru' ? 'selected' : ''}>Русский</option>
    </select>
</form>