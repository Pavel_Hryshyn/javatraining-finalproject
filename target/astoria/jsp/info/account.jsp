<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib prefix="tg" uri="tags" %>

<html lang="${language}"><head>

<title><fmt:message key="jsp.title.account" /></title></head>
<body>
	<jsp:include page="/jsp/header/header.jsp"></jsp:include>
	<jsp:include page="/jsp/menu/menu.jsp"></jsp:include>

		<c:if test="${user.admin}">
			<jsp:include page="/jsp/info/admin_account.jsp"></jsp:include>
		</c:if>
		<c:if test="${not empty user and not user.admin}">
			<jsp:include page="/jsp/info/user_account.jsp"></jsp:include>
		</c:if>

<div class="panel panel-warning">
	<div class="panel-footer"><jsp:include page="/jsp/header/footer.jsp"></jsp:include></div>
</div>
</body>