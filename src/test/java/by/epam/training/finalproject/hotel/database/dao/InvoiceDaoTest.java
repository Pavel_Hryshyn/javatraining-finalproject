/**
 * 
 */
package by.epam.training.finalproject.hotel.database.dao;

import static org.junit.Assert.*;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

import org.junit.BeforeClass;
import org.junit.Ignore;
import org.junit.Test;

import by.epam.training.finalproject.hotel.database.dao.exceptions.DaoTechException;
import by.epam.training.finalproject.hotel.i18n.MessageManager;
import by.epam.training.finalproject.hotel.model.entities.HotelRoom;
import by.epam.training.finalproject.hotel.model.entities.HotelRoomType;
import by.epam.training.finalproject.hotel.model.entities.Invoice;

/**
 * @author Pavel Hryshyn
 *
 */
public class InvoiceDaoTest {
	private final static String PATTERN_FORMAT_DATE = "pattern.format.date";
	
	private InvoiceDao invoiceDao = new InvoiceDao();
	private static BookingDao bookingDao = new BookingDao();
	private static HotelRoomDao roomDao = new HotelRoomDao();
	private static Invoice invoice1;
	private static Invoice invoice2;
	
	@BeforeClass
	public static void init() throws DaoTechException{
		invoice1 = new Invoice();
		invoice1.setId(1);
		invoice1.setTotalAmount(60);
		invoice1.setBooking(bookingDao.findById(1));
		invoice1.setHotelRoom(roomDao.findById(101));
		
		invoice2 = new Invoice();
		invoice2.setTotalAmount(240);
		invoice2.setBooking(bookingDao.findById(2));
		invoice2.setHotelRoom(roomDao.findById(103));
	}
	
	//delete Ignore before test
	@Ignore
	@Test
	public void testFindAll() throws DaoTechException{
		List<Invoice> invoiceList = invoiceDao.findAll();
		
		assertEquals("List size should be equals", 1, invoiceList.size());
		assertEquals("This booking should be some", invoice1, invoiceList.get(0));
	}
	
	//delete Ignore before test
	@Ignore
	@Test
	public void testFindById() throws DaoTechException{
		Invoice actInvoice = invoiceDao.findById(1);
		
		assertEquals("This booking should be some", invoice1, actInvoice);
	}
	
	//delete Ignore before test
	@Ignore
	@Test
	public void testFindInvoiceByUserId() throws DaoTechException{
		List<Invoice> invoiceListUser2 = invoiceDao.findInvoiceByUserId(2);
		assertTrue(invoiceListUser2.isEmpty());
		
		List<Invoice> invoiceListUser1 = invoiceDao.findInvoiceByUserId(1);
		assertEquals("List size should be equals", 1, invoiceListUser1.size());
		assertEquals("This booking should be some", invoice1, invoiceListUser1.get(0));
	}
	
	//delete Ignore before test
	@Ignore
	@Test
	public void testFindInvoiceByBookingId() throws DaoTechException{
		Invoice actInvoiceEmpty = invoiceDao.findInvoiceByBookingId(2);
		assertNull(actInvoiceEmpty);
		
		Invoice actInvoice = invoiceDao.findInvoiceByBookingId(1);
		assertEquals("This booking should be some", invoice1, actInvoice);
	}
	
	//delete Ignore before test
	@Ignore
	@Test
	public void testFindNonFreeRoomBetweenDates() throws DaoTechException, ParseException{
		Date startDateOuter = new SimpleDateFormat(MessageManager.getValue(PATTERN_FORMAT_DATE))
						.parse("2015-07-08");
		Date endDateOuter = new SimpleDateFormat(MessageManager.getValue(PATTERN_FORMAT_DATE))
						.parse("2015-07-09");
		Date startDateInner1 = new SimpleDateFormat(MessageManager.getValue(PATTERN_FORMAT_DATE))
						.parse("2015-07-03");
		Date endDateInner1 = new SimpleDateFormat(MessageManager.getValue(PATTERN_FORMAT_DATE))
						.parse("2015-07-09");
		Date startDateInner2 = new SimpleDateFormat(MessageManager.getValue(PATTERN_FORMAT_DATE))
						.parse("2015-07-01");
		Date endDateInner2 = new SimpleDateFormat(MessageManager.getValue(PATTERN_FORMAT_DATE))
						.parse("2015-07-09");
		Date startDateInner3 = new SimpleDateFormat(MessageManager.getValue(PATTERN_FORMAT_DATE))
						.parse("2015-07-03");
		Date endDateInner3 = new SimpleDateFormat(MessageManager.getValue(PATTERN_FORMAT_DATE))
						.parse("2015-07-03");
		
		List<HotelRoom> nonFreeRoomEmptyList = invoiceDao.findNonFreeRoomBetweenDates(startDateOuter, endDateOuter);
		assertTrue(nonFreeRoomEmptyList.isEmpty());
		
		List<HotelRoom> nonFreeRoomList1 = invoiceDao.findNonFreeRoomBetweenDates(startDateInner1, endDateInner1);
		assertEquals("List size should be equals", 1, nonFreeRoomList1.size());
		assertEquals("This booking should be some", invoice1.getHotelRoom(), nonFreeRoomList1.get(0));
		
		
		List<HotelRoom> nonFreeRoomList2 = invoiceDao.findNonFreeRoomBetweenDates(startDateInner2, endDateInner2);
		assertEquals("List size should be equals", 1, nonFreeRoomList2.size());
		assertEquals("This booking should be some", invoice1.getHotelRoom(), nonFreeRoomList2.get(0));
		
		List<HotelRoom> nonFreeRoomList3 = invoiceDao.findNonFreeRoomBetweenDates(startDateInner3, endDateInner3);
		assertEquals("List size should be equals", 1, nonFreeRoomList3.size());
		assertEquals("This booking should be some", invoice1.getHotelRoom(), nonFreeRoomList3.get(0));
	}
	
	//delete Ignore before test
	@Ignore
	@Test
	public void testFindNonFreeRoomBetweenDatesByTypes() throws DaoTechException, ParseException{
		Date startDateOuter = new SimpleDateFormat(MessageManager.getValue(PATTERN_FORMAT_DATE))
						.parse("2015-07-08");
		Date endDateOuter = new SimpleDateFormat(MessageManager.getValue(PATTERN_FORMAT_DATE))
						.parse("2015-07-09");
		Date startDateInner1 = new SimpleDateFormat(MessageManager.getValue(PATTERN_FORMAT_DATE))
						.parse("2015-07-03");
		Date endDateInner1 = new SimpleDateFormat(MessageManager.getValue(PATTERN_FORMAT_DATE))
						.parse("2015-07-09");
		Date startDateInner2 = new SimpleDateFormat(MessageManager.getValue(PATTERN_FORMAT_DATE))
						.parse("2015-07-01");
		Date endDateInner2 = new SimpleDateFormat(MessageManager.getValue(PATTERN_FORMAT_DATE))
						.parse("2015-07-09");
		Date startDateInner3 = new SimpleDateFormat(MessageManager.getValue(PATTERN_FORMAT_DATE))
						.parse("2015-07-03");
		Date endDateInner3 = new SimpleDateFormat(MessageManager.getValue(PATTERN_FORMAT_DATE))
						.parse("2015-07-03");
		
		List<HotelRoom> nonFreeRoomEmptyList = invoiceDao.findNonFreeRoomBetweenDatesByTypes(startDateOuter, endDateOuter, HotelRoomType.BEDROOM);
		assertTrue(nonFreeRoomEmptyList.isEmpty());
		
		List<HotelRoom> nonFreeRoomEmptyList1 = invoiceDao.findNonFreeRoomBetweenDatesByTypes(startDateInner1, endDateInner1, HotelRoomType.BEDROOM);
		assertTrue(nonFreeRoomEmptyList1.isEmpty());
		
		List<HotelRoom> nonFreeRoomEmptyList2 = invoiceDao.findNonFreeRoomBetweenDatesByTypes(startDateInner2, endDateInner2, HotelRoomType.BEDROOM);
		assertTrue(nonFreeRoomEmptyList2.isEmpty());
		
		List<HotelRoom> nonFreeRoomEmptyList3 = invoiceDao.findNonFreeRoomBetweenDatesByTypes(startDateInner3, endDateInner3, HotelRoomType.BEDROOM);
		assertTrue(nonFreeRoomEmptyList3.isEmpty());
		
		List<HotelRoom> nonFreeRoomList1 = invoiceDao.findNonFreeRoomBetweenDatesByTypes(startDateInner1, endDateInner1, HotelRoomType.STANDART);
		assertEquals("List size should be equals", 1, nonFreeRoomList1.size());
		assertEquals("This booking should be some", invoice1.getHotelRoom(), nonFreeRoomList1.get(0));
		
		List<HotelRoom> nonFreeRoomList2 = invoiceDao.findNonFreeRoomBetweenDatesByTypes(startDateInner2, endDateInner2, HotelRoomType.STANDART);
		assertEquals("List size should be equals", 1, nonFreeRoomList2.size());
		assertEquals("This booking should be some", invoice1.getHotelRoom(), nonFreeRoomList2.get(0));

		List<HotelRoom> nonFreeRoomList3 = invoiceDao.findNonFreeRoomBetweenDatesByTypes(startDateInner3, endDateInner3, HotelRoomType.STANDART);
		assertEquals("List size should be equals", 1, nonFreeRoomList3.size());
		assertEquals("This booking should be some", invoice1.getHotelRoom(), nonFreeRoomList3.get(0));
	}
	
	//delete Ignore before test
	@Ignore
	@Test
	public void testAddUpdateDelete() throws DaoTechException{
		Invoice invoiceBeforeAdd = invoiceDao.findInvoiceByBookingId(2);
		assertNull(invoiceBeforeAdd);
		
		invoiceDao.add(invoice2);
		Invoice invoiceAfterAdd = invoiceDao.findInvoiceByBookingId(2);
		invoice2.setId(invoiceAfterAdd.getId());
		assertEquals("This booking should be some", invoice2, invoiceAfterAdd);
		
		invoice2.setInvoiceComment("Comment");
		invoiceDao.update(invoice2);
		Invoice invoiceAfterUpdate = invoiceDao.findInvoiceByBookingId(2);
		invoice2.setId(invoiceAfterUpdate.getId());
		assertEquals("This booking should be some", invoice2, invoiceAfterUpdate);
		
		invoiceDao.delete(invoice2.getId());
		Invoice invoiceAfterDelete = invoiceDao.findInvoiceByBookingId(2);
		assertNull(invoiceAfterDelete);
	}
}
