package by.epam.training.finalproject.hotel.database.dbconnection;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;

public class DBConnector {
	private static final String URL = "database.url";
	private static final String USER = "database.user";
	private static final String PASSWORD = "database.password";
	private static Connection connection;
	
	private static String url = TestDBResourcesManager.getInstance().getValue(URL);
    private static String user = TestDBResourcesManager.getInstance().getValue(USER);
    private static String pass = TestDBResourcesManager.getInstance().getValue(PASSWORD);
	
	public static Connection getConnection() {
		try {
			connection = DriverManager.getConnection(url, user, pass);
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return connection;
	}
	
}
