package runner;

import java.util.Date;
import java.util.List;

import by.epam.training.finalproject.hotel.database.dao.BookingDao;
import by.epam.training.finalproject.hotel.database.dao.HotelRoomDao;
import by.epam.training.finalproject.hotel.database.dao.InvoiceDao;
import by.epam.training.finalproject.hotel.database.dao.UserDao;
import by.epam.training.finalproject.hotel.database.dao.exceptions.DaoTechException;
import by.epam.training.finalproject.hotel.model.entities.Booking;
import by.epam.training.finalproject.hotel.model.entities.HotelRoom;
import by.epam.training.finalproject.hotel.model.entities.HotelRoomType;
import by.epam.training.finalproject.hotel.model.entities.User;
import by.epam.training.finalproject.hotel.model.logic.FindFreeRooms;

public class Runner {

	public static void main(String[] args) {
		UserDao userDao = new UserDao();
		try {
//			User user2 = new User();
//			user2.setLogin("пряник");
//			user2.setPassword("123456");
//			user2.setFirstName("Страус");
//			user2.setLastName("Вася");
//			user2.setEmail("vasya@mail.ru");
//	//		boolean isAdd = userDao.add(user2);
	//		System.out.print(isAdd);
	//		userDao.delete(5);
//			User updateUser = userDao.findById(6);
//			updateUser.setFirstName("Иван");
//			updateUser.setLastName("Петров");
//			userDao.update(updateUser);
//			
//			List<User> userList = userDao.findAll();
//			for (User user:userList){
//				System.out.println(user);
//			}
//			
			
////			User user = userDao.findUserByLoginPassword("santa", "1234");
//			System.out.println(user);
//			User user1 = userDao.findById(5);
//			System.out.println(user1);
			
//			List<User> userList1 = userDao.findUserByLastName("Aдмин");
//			for (User user:userList1){
//				System.out.println(user);
//			}
	
			HotelRoomDao roomDao = new HotelRoomDao();
//			List<HotelRoom> rooms = roomDao.findAll();
//			for (HotelRoom room:rooms){
//				System.out.println(room);
//			}
			List<HotelRoom> roomsByType = roomDao.findAllRoomsByRoomType(HotelRoomType.STANDART);
			for (HotelRoom room:roomsByType){
				System.out.println(room);
			}
			
//			List<HotelRoom> roomsToMax = roomDao.findAllRoomsToMaxPrice(50.0);
//			for (HotelRoom room:roomsToMax){
//				System.out.println(room);
//			}
			
//			List<HotelRoom> roomsFromMin = roomDao.findAllRoomsFromMinPrice(50.0);
//			for (HotelRoom room:roomsFromMin){
//				System.out.println(room);
//			}
			
			BookingDao bookingDao = new BookingDao();
			List<Booking> bookingList = bookingDao.findAll();
			for (Booking booking:bookingList){
				System.out.println(booking);
			}
			
			InvoiceDao invoiceDao = new InvoiceDao();
			Date startDate = new Date(2015, 06, 01);
			Date endDate = new Date(2015, 06, 07);
			List<HotelRoom> roomsList = invoiceDao.findNonFreeRoomBetweenDatesByTypes(startDate, endDate, HotelRoomType.STANDART);
			
			List<HotelRoom> freeRoomList = FindFreeRooms.find(roomsByType, roomsList);
			
			for (HotelRoom room:freeRoomList){
				System.out.println(room);
			}
			
			Double price = roomDao.findRoomPriceByRoomType(HotelRoomType.STANDART);
			System.out.println(price);
			
		} catch (DaoTechException e) {
			System.err.print(e);
		}
		

	}

}
