package by.epam.training.finalproject.hotel.exceptions;

public class DaoLogicException extends Exception {
	private static final long serialVersionUID = 1987248679026951694L;

	public DaoLogicException(String message, Throwable cause) {
		super(message, cause);
	}
	
}	
