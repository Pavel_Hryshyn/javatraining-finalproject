/*
 * @(#)Controller.java   1.0 2015/05/20
 */
package by.epam.training.finalproject.hotel.web.controller;

import java.io.IOException;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.log4j.Logger;

import by.epam.training.finalproject.hotel.i18n.MessageManager;
import by.epam.training.finalproject.hotel.web.command.Command;
import by.epam.training.finalproject.hotel.web.command.CommandFactory;
import by.epam.training.finalproject.hotel.web.util.PageConfigurationManager;

/**
 * This servlet handles client requests 
 * @version 1.0 20 May 2015
 * @author Pavel Hryshyn
 */
@WebServlet("/controller")
public class Controller extends HttpServlet {
	private final static long serialVersionUID = 5420803240588548208L;
	
	/** This object obtains logger for this class*/
	private final static Logger logger = Logger.getLogger(Controller.class);
	
	/** This constants store path to jsp pages */
	private final static String PATH_PAGE_ERROR_404 = "path.page.error404";
	
	/** This constants store logger messages */
	private final static String LOGGER_MSG_PAGE_NOT_FOUND = "logger.message.command.page.not.found";
	
	/**
	 * This method gets data from server
	 * @param req
	 * 			this parameter is request to server
	 * @param resp
	 * 			this parameter is response to server
	 * @throws ServletException, IOException
	 */
	@Override
	protected void doGet(HttpServletRequest req, HttpServletResponse resp)
			throws ServletException, IOException {
		processRequest(req, resp);
	}

	/**
	 * This method sends data to server
	 * @param req
	 * 			this parameter is request to server
	 * @param resp
	 * 			this parameter is response to server
	 * @throws ServletException, IOException
	 */
	@Override
	protected void doPost(HttpServletRequest req, HttpServletResponse resp)
			throws ServletException, IOException {
		processRequest(req, resp);
	}
	
	/**
	 * This method hands commands
	 * @param req
	 * 			this parameter is request to server
	 * @param resp
	 * 			this parameter is response to server
	 * @throws ServletException, IOException
	 */
	private void processRequest(HttpServletRequest req, HttpServletResponse resp)
			throws ServletException, IOException {
		String page = null;
		CommandFactory client = new CommandFactory();
		Command command = client.defineCommand(req);
		page = command.execute(req);
		if (page != null) {
			RequestDispatcher dispatcher = getServletContext().getRequestDispatcher(page);
			dispatcher.forward(req, resp);
		} else {
			logger.debug(MessageManager.getValue(LOGGER_MSG_PAGE_NOT_FOUND) + command.toString());
			RequestDispatcher dispatcher = getServletContext().getRequestDispatcher(PageConfigurationManager.getValue(PATH_PAGE_ERROR_404));
			dispatcher.forward(req, resp);
			//resp.sendRedirect(req.getContextPath() + page);
		}
	}	
}
