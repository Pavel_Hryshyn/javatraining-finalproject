/*
 * @(#)RegistrationPageCommand.java   1.0 2015/06/14
 */
package by.epam.training.finalproject.hotel.web.command.navigation;

import javax.servlet.http.HttpServletRequest;

import org.apache.log4j.Logger;

import by.epam.training.finalproject.hotel.i18n.MessageManager;
import by.epam.training.finalproject.hotel.web.command.Command;
import by.epam.training.finalproject.hotel.web.util.PageConfigurationManager;

/**
 * This class implements command pattern and forwards to registration page
 * @version 1.0 14 June 2015
 * @author Pavel Hryshyn
 */
public class RegistrationPageCommand extends Command {
	/** This object obtains logger for this class*/
	private final static Logger logger = Logger.getLogger(RegistrationPageCommand.class);
	
	/** This constants store path to jsp pages */
	private final static String PATH_REGISTRATION_PAGE = "path.page.registration";
	
	/** This constants store logger messages */
	private final static String LOGGER_MSG_REGISTRATION_PAGE = "logger.message.command.page.registration";
	
	/**
	 * This method forwards to registration page if account page not found then return error 404 page
	 * @param request
	 * @return page where request is forwarded
	 */
	@Override
	public String execute(HttpServletRequest request) {
		String page = PageConfigurationManager.getValue(PATH_REGISTRATION_PAGE);
		logger.info(MessageManager.getValue(LOGGER_MSG_REGISTRATION_PAGE));
		return page;
	}

}
