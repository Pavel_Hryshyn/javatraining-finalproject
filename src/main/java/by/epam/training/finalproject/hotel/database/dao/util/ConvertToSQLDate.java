/*
 * @(#)ConvertToSQLDate.java   1.0 2015/06/20
 */
package by.epam.training.finalproject.hotel.database.dao.util;

import java.util.Date;

/**
 * This class contains method to convert java.util.Date to java.sql.Date
 * @version 1.0 20 June 2015
 * @author Pavel Hryshyn
 */
public class ConvertToSQLDate {
	
	/**
	 * This method converts java.util.Date to java.sql.Date
	 * @param date
	 * 			the param is java.util.Date
	 * @return java.sql.Date
	 */
	public static java.sql.Date convert(Date date){
		java.sql.Date sqlDate = null;
		if (date != null){
			sqlDate = new java.sql.Date(date.getTime());
		}
		return sqlDate;
	}
}
