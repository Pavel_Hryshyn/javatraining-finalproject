/*
 * @(#)Command.java   1.0 2015/05/30
 */
package by.epam.training.finalproject.hotel.web.command;

import javax.servlet.http.HttpServletRequest;

/**
 * This is a superclass for all classes of Command pattern and holds method of executing command
 * @version 1.0 30 May 2015
 * @author Pavel Hryshyn
 */
public abstract class Command {	
	abstract public String execute(HttpServletRequest request);
}
