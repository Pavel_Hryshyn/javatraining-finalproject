/*
 * @(#)IInvoiceDao.java   1.2 2015/06/20
 */
package by.epam.training.finalproject.hotel.database.dao.interfaces;

import java.util.Date;
import java.util.List;
import by.epam.training.finalproject.hotel.database.dao.exceptions.DaoTechException;
import by.epam.training.finalproject.hotel.model.entities.HotelRoom;
import by.epam.training.finalproject.hotel.model.entities.HotelRoomType;
import by.epam.training.finalproject.hotel.model.entities.Invoice;

/**
 * This interface defines methods that bind the Invoice entity with database
 * @version 1.2 20 June 2015
 * @author Pavel Hryshyn
 */
public interface IInvoiceDao extends ICommonDao<Integer, Invoice> {
	List<Invoice> findInvoiceByUserId(Integer id) throws DaoTechException;
	Invoice findInvoiceByBookingId(Integer id) throws DaoTechException;
	List<HotelRoom> findNonFreeRoomBetweenDates(Date startDate, Date endDate) throws DaoTechException;
	List<HotelRoom> findNonFreeRoomBetweenDatesByTypes(Date startDate, Date endDate, HotelRoomType type) throws DaoTechException;
}
