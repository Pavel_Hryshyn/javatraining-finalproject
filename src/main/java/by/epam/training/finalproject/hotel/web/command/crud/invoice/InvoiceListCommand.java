/*
 * @(#)InvoiceListCommand.java   1.0 2015/06/18
 */
package by.epam.training.finalproject.hotel.web.command.crud.invoice;

import java.util.List;

import javax.servlet.http.HttpServletRequest;

import org.apache.log4j.Logger;

import by.epam.training.finalproject.hotel.database.dao.InvoiceDao;
import by.epam.training.finalproject.hotel.database.dao.exceptions.DaoTechException;
import by.epam.training.finalproject.hotel.i18n.MessageManager;
import by.epam.training.finalproject.hotel.model.entities.Invoice;
import by.epam.training.finalproject.hotel.web.command.Command;
import by.epam.training.finalproject.hotel.web.util.PageConfigurationManager;

/**
 * This class implements command pattern and gets list of invoices from database
 * @version 1. 18 June 2015
 * @author Pavel Hryshyn
 */
public class InvoiceListCommand extends Command {
	/** This object obtains logger for this class*/
	private final static Logger logger = Logger.getLogger(InvoiceListCommand.class);
	
	/** This constants store names of request parameters */
	private final static String PARAM_INVOICE_LIST = "invoiceList";
	private final static String PARAM_INVOICE_LIST_EMPTY = "invoiceListEmpty";
	
	/** This constants store path to jsp pages */
	private final static String PATH_INVOICE_LIST = "path.page.invoice.list";
	private final static String PATH_PAGE_ERROR = "path.page.error";
	
	/** This constants store messages that output to jsp page */
	private final static String MSG_INVOICE_LIST_EMPTY = "jsp.message.invoice.list.empty";
	
	/** This constants store logger messages */
	private final static String LOGGER_MSG_INVOICE_LIST_GET = "logger.message.command.invoice.list.get";
	private final static String LOGGER_MSG_INVOICE_LIST_EMPTY = "logger.message.command.invoice.list.empty";

	private InvoiceDao invoiceDao = new InvoiceDao();
	
	/**
	 * This method gets list of invoices from database
	 * @param request
	 * @return page where request is forwarded
	 */
	@Override
	public String execute(HttpServletRequest request) {
		String page = null;
		try {
			List<Invoice> invoiceList = invoiceDao.findAll();
			if (invoiceList != null){
				request.setAttribute(PARAM_INVOICE_LIST, invoiceList);
				logger.info(MessageManager.getValue(LOGGER_MSG_INVOICE_LIST_GET));
			} else {
				request.setAttribute(PARAM_INVOICE_LIST_EMPTY, MessageManager.getValue(MSG_INVOICE_LIST_EMPTY));
				logger.info(MessageManager.getValue(LOGGER_MSG_INVOICE_LIST_EMPTY));
			}		
			page = PageConfigurationManager.getValue(PATH_INVOICE_LIST);
		} catch (DaoTechException e) {
			logger.debug(e);
			page = PageConfigurationManager.getValue(PATH_PAGE_ERROR);
			return page;
		}
		return page;
	}
}
