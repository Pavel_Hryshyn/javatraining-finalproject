/*
 * @(#)DBConnectionPool.java   1.2 2015/05/15
 */
package by.epam.training.finalproject.hotel.database.dbconnection;

import java.sql.Array;
import java.sql.Blob;
import java.sql.CallableStatement;
import java.sql.Clob;
import java.sql.Connection;
import java.sql.DatabaseMetaData;
import java.sql.DriverManager;
import java.sql.NClob;
import java.sql.PreparedStatement;
import java.sql.SQLClientInfoException;
import java.sql.SQLException;
import java.sql.SQLWarning;
import java.sql.SQLXML;
import java.sql.Savepoint;
import java.sql.Statement;
import java.sql.Struct;
import java.util.Locale;
import java.util.Map;
import java.util.Properties;
import java.util.concurrent.ArrayBlockingQueue;
import java.util.concurrent.BlockingQueue;
import java.util.concurrent.Executor;
import java.util.concurrent.locks.Lock;
import java.util.concurrent.locks.ReentrantLock;

import org.apache.log4j.Logger;

import by.epam.training.finalproject.hotel.database.dbconnection.exception.DBConnectionException;
import by.epam.training.finalproject.hotel.i18n.MessageManager;

/**
 * This class implements connection pool with database
 * @version 1.2 15 May 2015
 * @author Pavel Hryshyn
 */
public class DBConnectionPool {
	/** This object obtains logger for this class*/
	private final static Logger logger = Logger.getLogger(DBConnectionPool.class);
	
	/** This constants store parameters of database connection */
	private final static String DATABASE_DRIVER = "database.driver";
	private final static String DATABASE_URL = "database.url";
	private final static String DATABASE_USER = "database.user";
	private final static String DATABASE_PASSWORD = "database.password";
	private final static String DATABASE_POOL_SIZE = "db.poolsize";
	
	/** This constants store logger messages */
	private final static String LOGGER_POOL_CREATED = "dbpool.logger.pool.created";
	private final static String LOGGER_INIT_SUCCESS = "dbpool.logger.init.success";
	private final static String LOGGER_QUEUE_CLEAR = "dbpool.logger.queue.clear";
		
	/** This constants store exception messages */
	private final static String EXC_INIT_SQL = "dbpool.exception.init.sql";
	private final static String EXC_INIT_DRIVER = "dbpool.exception.init.driver";
	private final static String EXC_QUEUE_CLEAR = "dbpool.exception.queue.clear";
	private final static String EXC_CONNECT_GOT = "dbpool.exception.connect.got";
	private final static String EXC_CONNECT_RETURN = "dbpool.exception.connect.return";
	private final static String EXC_CONNECT_ERROR_CLOSE = "dbpool.exception.connect.error.close";
	private final static String EXC_CONNECT_ERROR_REMOVE = "dbpool.exception.connect.error.remove";
	
	/** This field is queue of free connection */
	private BlockingQueue<Connection> connectionQueue;
	
	/** This field is queue of used connection */
	private BlockingQueue<Connection> givenAwayConQueue; 
	
	/** This field is instance of connection pool */
	private static DBConnectionPool instance = null;
	
	/** This field is flag to indicate if connection pool is created */
	private static volatile boolean instanceCreated = false;
	
	/** This fields are parameters of database connection */
	private String driverName; 
	private String url; 
	private String user; 
	private String password; 
	private int poolSize; 
	
	private DBConnectionPool() {
		DBResourceManager dbResourceManager = DBResourceManager.getInstance();
		this.driverName = dbResourceManager.getValue(DATABASE_DRIVER);
		this.url = dbResourceManager.getValue(DATABASE_URL);
		this.user = dbResourceManager.getValue(DATABASE_USER);
		this.password = dbResourceManager.getValue(DATABASE_PASSWORD);
		try {
			this.poolSize = Integer.parseInt(dbResourceManager.
					getValue(DATABASE_POOL_SIZE)); 
		} catch (NumberFormatException e) {
			poolSize = 10;
		}
	} 
	
	/**
	 * This method initializes connection pool
	 * @throws DBConnectionException, if arise database access error or other database errors
	 * or can't find database driver
	 */
	public void initDBPool() throws DBConnectionException {
		Locale.setDefault(Locale.ENGLISH);
		try {
			Class.forName(driverName);
			givenAwayConQueue = new ArrayBlockingQueue<Connection>(poolSize);
			connectionQueue = new ArrayBlockingQueue<Connection>(poolSize);
			for (int i = 0; i < poolSize; i++){
				Connection connection = DriverManager.getConnection(url, user, password);
				DBPooledConnection pooledConnection = new DBPooledConnection(connection); 
			    connectionQueue.add(pooledConnection); 
			}
			logger.info(MessageManager.getValue(LOGGER_INIT_SUCCESS));
		} catch (SQLException e) {
			throw new DBConnectionException(MessageManager.getValue(EXC_INIT_SQL), e);
		} catch (ClassNotFoundException e) {
			throw new DBConnectionException(MessageManager.getValue(EXC_INIT_DRIVER), e);
		}
	}
	
	/**
	 * This method gets instance of connection pool if it isn't exist that creates connection pool 
	 * @throws DBConnectionException, if initialization of connection pool is failed
	 */
	public static DBConnectionPool getInstance() throws DBConnectionException {
		final Lock lockPool = new ReentrantLock(); 
		if (!instanceCreated){
			lockPool.lock();
			if (!instanceCreated) {
			instance = new DBConnectionPool();
			instance.initDBPool();
			instanceCreated = true;
			logger.info(MessageManager.getValue(LOGGER_POOL_CREATED));
			}
			lockPool.unlock();
		}
		return instance;
	}

	/**
	 * This method clears givenAwayConQueue and connectionQueue
	 * @throws DBConnectionException, if connection queues can't clear
	 */
	public void dispose() throws DBConnectionException { 
			clearConnectionQueue(); 
	} 

	/**
	 * This method clears givenAwayConQueue and connectionQueue
	 * @throws DBConnectionException, if connection queues can't clear
	 */
	private void clearConnectionQueue() throws DBConnectionException { 
		try { 
			closeConnectionsQueue(givenAwayConQueue); 
			closeConnectionsQueue(connectionQueue); 
			logger.debug(MessageManager.getValue(LOGGER_QUEUE_CLEAR));
		} catch (SQLException e) { 
			throw new DBConnectionException(MessageManager.getValue(EXC_QUEUE_CLEAR), e); 
		} 
	} 

	/**
	 * This method gets connection from connection pool
	 * @throws DBConnectionException, if interrupted while waiting connection
	 */
	public Connection takeConnection() throws DBConnectionException { 
		Connection connection = null; 
		try { 
			connection = connectionQueue.take(); 
			givenAwayConQueue.add(connection); 
		} catch (InterruptedException e) { 
			throw new DBConnectionException(MessageManager.getValue(EXC_CONNECT_GOT), e); 
		} 
		return connection; 
	} 
	
	/**
	 * This method returns connection to connection pool
	 * @throws DBConnectionException, if arise database access error or other database errors
	 */
	public void closeConnection(Connection con) throws DBConnectionException{
		try {
			con.close();
		} catch (SQLException e) {
			throw new DBConnectionException(MessageManager.getValue(EXC_CONNECT_RETURN), e);
		}
	}
	
	/**
	 * This method removes all connection from connection queue
	 * @param queue
	 * 			the param is connection queue
	 * @throws SQLException, if arise error of closing connection
	 */
	private void closeConnectionsQueue(BlockingQueue<Connection> queue) throws SQLException{
		Connection connection;
		while ((connection = queue.poll()) != null){
			if (!connection.getAutoCommit()){
				connection.commit();
			}
			((DBPooledConnection) connection).reallyClose();
		}
	}
	
	/**
	 * This class contains methods of managing of pooled connection
	 */
	private class DBPooledConnection implements Connection{
		private Connection connection;
		
		public DBPooledConnection(Connection connection) throws SQLException {
			this.connection = connection;
			this.connection.setAutoCommit(true);
		}
		
		/**
		 * This method closes connection
		 * @throws SQLException, if arise error of closing connection
		 */
		@Override
		 public void close() throws SQLException {
				if (connection.isClosed()){
					throw new SQLException(MessageManager.getValue(EXC_CONNECT_ERROR_CLOSE));
				}
				if (connection.isReadOnly()) { 
				        connection.setReadOnly(false); 
				      } 
				if (!givenAwayConQueue.remove(this)) { 
				        throw new SQLException(MessageManager.getValue(EXC_CONNECT_ERROR_REMOVE)); 
				      } 
				 if (!connectionQueue.offer(this)) { 
				        throw new SQLException(MessageManager.getValue(EXC_CONNECT_RETURN)); 
				      } 	
			}
		
		 public void reallyClose() throws SQLException { 
		      connection.close(); 
		    } 
		 
		 @Override
		 public void clearWarnings() throws SQLException { 
		      connection.clearWarnings(); 
		    }
		 
		 @Override
		 public void commit() throws SQLException { 
		      connection.commit(); 
		    }

		@Override
		public <T> T unwrap(Class<T> iface) throws SQLException {
			return connection.unwrap(iface);
		}

		@Override
		public boolean isWrapperFor(Class<?> iface) throws SQLException {
			return connection.isWrapperFor(iface);
		}

		@Override
		public Statement createStatement() throws SQLException {
			return connection.createStatement();
		}

		@Override
		public PreparedStatement prepareStatement(String sql)
				throws SQLException {
			return connection.prepareStatement(sql);
		}

		@Override
		public CallableStatement prepareCall(String sql) throws SQLException {
			return connection.prepareCall(sql);
		}

		@Override
		public String nativeSQL(String sql) throws SQLException {
			return connection.nativeSQL(sql);
		}

		@Override
		public void setAutoCommit(boolean autoCommit) throws SQLException {
			connection.setAutoCommit(autoCommit);
			
		}

		@Override
		public boolean getAutoCommit() throws SQLException {
			return connection.getAutoCommit();
		}

		@Override
		public void rollback() throws SQLException {
			connection.rollback();
			
		}

		@Override
		public boolean isClosed() throws SQLException {
			return connection.isClosed();
		}

		@Override
		public DatabaseMetaData getMetaData() throws SQLException {
			return connection.getMetaData();
		}

		@Override
		public void setReadOnly(boolean readOnly) throws SQLException {
			connection.setReadOnly(readOnly);
			
		}

		@Override
		public boolean isReadOnly() throws SQLException {
			return connection.isReadOnly();
		}

		@Override
		public void setCatalog(String catalog) throws SQLException {
			connection.setCatalog(catalog);
			
		}

		@Override
		public String getCatalog() throws SQLException {
			return connection.getCatalog();
		}

		@Override
		public void setTransactionIsolation(int level) throws SQLException {
			connection.setTransactionIsolation(level);
			
		}

		@Override
		public int getTransactionIsolation() throws SQLException {
			return connection.getTransactionIsolation();
		}

		@Override
		public SQLWarning getWarnings() throws SQLException {
			return connection.getWarnings();
		}


		@Override
		public Statement createStatement(int resultSetType, int resultSetConcurrency)
				throws SQLException {
			return connection.createStatement(resultSetType, resultSetConcurrency);
		}

		@Override
		public PreparedStatement prepareStatement(String sql, int resultSetType,
				int resultSetConcurrency) throws SQLException {
			return connection.prepareStatement(sql, resultSetType, resultSetConcurrency);
		}

		@Override
		public CallableStatement prepareCall(String sql, int resultSetType,
				int resultSetConcurrency) throws SQLException {
			return connection.prepareCall(sql, resultSetType, resultSetConcurrency);
		}

		@Override
		public Map<String, Class<?>> getTypeMap() throws SQLException {
			return connection.getTypeMap();
		}

		@Override
		public void setTypeMap(Map<String, Class<?>> map) throws SQLException {
			connection.setTypeMap(map);
		}

		@Override
		public void setHoldability(int holdability) throws SQLException {
			connection.setHoldability(holdability);
		}

		@Override
		public int getHoldability() throws SQLException {
			return connection.getHoldability();
		}

		@Override
		public Savepoint setSavepoint() throws SQLException {
			return connection.setSavepoint();
		}

		@Override
		public Savepoint setSavepoint(String name) throws SQLException {
			return connection.setSavepoint(name);
		}

		@Override
		public void rollback(Savepoint savepoint) throws SQLException {
			connection.rollback(savepoint);
			
		}

		@Override
		public void releaseSavepoint(Savepoint savepoint) throws SQLException {
			connection.releaseSavepoint(savepoint);
		}

		@Override
		public Statement createStatement(int resultSetType,
				int resultSetConcurrency, int resultSetHoldability)
				throws SQLException {
			return connection.createStatement();
		}

		@Override
		public PreparedStatement prepareStatement(String sql,
				int resultSetType, int resultSetConcurrency,
				int resultSetHoldability) throws SQLException {
			return connection.prepareStatement(sql, resultSetType, resultSetConcurrency, resultSetHoldability);
		}

		@Override
		public CallableStatement prepareCall(String sql, int resultSetType,
				int resultSetConcurrency, int resultSetHoldability)
				throws SQLException {
			return connection.prepareCall(sql, resultSetType, resultSetConcurrency, resultSetHoldability);
		}

		@Override
		public PreparedStatement prepareStatement(String sql,
				int autoGeneratedKeys) throws SQLException {
			return connection.prepareStatement(sql, autoGeneratedKeys);
		}

		@Override
		public PreparedStatement prepareStatement(String sql,
				int[] columnIndexes) throws SQLException {
			return connection.prepareStatement(sql, columnIndexes);
		}

		@Override
		public PreparedStatement prepareStatement(String sql,
				String[] columnNames) throws SQLException {
			return connection.prepareStatement(sql, columnNames);
		}

		@Override
		public Clob createClob() throws SQLException {
			return connection.createClob();
		}

		@Override
		public Blob createBlob() throws SQLException {
			return connection.createBlob();
		}

		@Override
		public NClob createNClob() throws SQLException {
			return connection.createNClob();
		}

		@Override
		public SQLXML createSQLXML() throws SQLException {
			return connection.createSQLXML();
		}

		@Override
		public boolean isValid(int timeout) throws SQLException {
			return connection.isValid(timeout);
		}

		@Override
		public void setClientInfo(String name, String value)
				throws SQLClientInfoException {
			connection.setClientInfo(name, value);
		}

		@Override
		public void setClientInfo(Properties properties)
				throws SQLClientInfoException {
			connection.setClientInfo(properties);
		}

		@Override
		public String getClientInfo(String name) throws SQLException {
			return connection.getClientInfo(name);
		}

		@Override
		public Properties getClientInfo() throws SQLException {
			return connection.getClientInfo();
		}

		@Override
		public Array createArrayOf(String typeName, Object[] elements)
				throws SQLException {
			return connection.createArrayOf(typeName, elements);
		}

		@Override
		public Struct createStruct(String typeName, Object[] attributes)
				throws SQLException {
			return connection.createStruct(typeName, attributes);
		}

		@Override
		public void setSchema(String schema) throws SQLException {
			connection.setSchema(schema);
		}

		@Override
		public String getSchema() throws SQLException {
			return connection.getSchema();
		}

		@Override
		public void abort(Executor executor) throws SQLException {
			connection.abort(executor);
		}

		@Override
		public void setNetworkTimeout(Executor executor, int milliseconds)
				throws SQLException {
			connection.setNetworkTimeout(executor, milliseconds);
		}

		@Override
		public int getNetworkTimeout() throws SQLException {
			return connection.getNetworkTimeout();
		} 
		 		 
	}

}

	