/*
 * @(#)RegistrationCommand.java   1.0 2015/06/02
 */
package by.epam.training.finalproject.hotel.web.command.auth;

import javax.servlet.http.HttpServletRequest;

import org.apache.log4j.Logger;

import by.epam.training.finalproject.hotel.database.dao.UserDao;
import by.epam.training.finalproject.hotel.database.dao.exceptions.DaoTechException;
import by.epam.training.finalproject.hotel.i18n.MessageManager;
import by.epam.training.finalproject.hotel.model.entities.User;
import by.epam.training.finalproject.hotel.web.command.Command;
import by.epam.training.finalproject.hotel.web.util.PageConfigurationManager;

/**
 * This class implements command pattern and registers user 
 * @version 1.0 02 June 2015
 * @author Pavel Hryshyn
 */
public class RegistrationCommand extends Command {
	/** This object obtains logger for this class*/
	private final static Logger logger = Logger.getLogger(RegistrationCommand.class);
	
	/** This constants store names of request parameters */
	private static final String LOGIN = "login";
	private static final String PASSWORD = "password";
	private static final String CONFIRM_PASSWORD = "confirmPassword";
	private static final String FIRST_NAME = "firstName";
	private static final String LAST_NAME = "lastName";
	private static final String EMAIL = "email";	
	private static final String PARAM_INCORRECT_LOGIN = "incorrectLogin";
	private static final String PARAM_NONCONFIRMED_PASSWORD = "nonConfirmPass";
	private static final String PARAM_INCORRECT_PASSWORD = "incorrectPass";
	private static final String PARAM_INCORRECT_FIRST_NAME = "incorrectFirstName";
	private static final String PARAM_INCORRECT_LAST_NAME = "incorrectLastName";
	private static final String PARAM_INCORRECT_EMAIL = "incorrectEmail";
	private static final String PARAM_USER_CREATED = "userCreated";
	private static final String PARAM_USER_EXIST = "userExist";
	
	/** This constants store path to jsp pages */
	private static final String PATH_PAGE_MAIN = "path.page.main";
	private static final String PATH_PAGE_REGISTRATION = "path.page.registration";
	private static final String PATH_PAGE_ERROR = "path.page.error";
	
	/** This constants store messages that output to jsp page */
	private static final String MSG_USER_EXIST = "jsp.message.user.exist";
	private static final String MSG_INCORRECT_LOGIN = "jsp.message.incorrect.login";
	private static final String MSG_NONCONFIRMED_PASSWORD = "jsp.message.nonconfirmed.pass";
	private static final String MSG_INCORRECT_PASSWORD = "jsp.message.incorrect.pass";
	private static final String MSG_INCORRECT_FIRST_NAME = "jsp.message.incorrect.first.name";
	private static final String MSG_INCORRECT_LAST_NAME = "jsp.message.incorrect.last.name";
	private static final String MSG_INCORRECT_EMAIL = "jsp.message.incorrect.email";
	private static final String MSG_USER_CREATED = "jsp.message.user.created";
	
	/** This constants store patterns for validation of registration form */
	private static final String PATTERN_LOGIN = "pattern.login";
	private static final String PATTERN_PASSWORD = "pattern.password";
	private static final String PATTERN_NAME = "pattern.name";
	private static final String PATTERN_EMAIL = "pattern.email";
		
	/** This constants store logger messages */
	private static final String LOGGER_MSG_COMMAND_USER_REGISTERED = "logger.message.command.user.registered";
	
	private UserDao userDao = new UserDao();
	private User user;
	
	/**
	 * This method registers user
	 * @param request
	 * @return page where request is forwarded
	 */
	@Override
	public String execute(HttpServletRequest request) {
		String page = null;
		user = new User();
		if (build(request)){
		try {
			if (userDao.findUserByLogin(user.getLogin()) == null) {
				userDao.add(user);
				request.setAttribute(PARAM_USER_CREATED, MessageManager.getValue(MSG_USER_CREATED));
				logger.info(MessageManager.getValue(LOGGER_MSG_COMMAND_USER_REGISTERED) + user.getLogin());
				page = PageConfigurationManager.getValue(PATH_PAGE_MAIN);
			} else {
				page = PageConfigurationManager.getValue(PATH_PAGE_REGISTRATION);
				request.setAttribute(PARAM_USER_EXIST, MessageManager.getValue(MSG_USER_EXIST));
				logger.debug(MessageManager.getValue(MSG_USER_EXIST));
			}
		} catch (DaoTechException e) {
			logger.error(e);
			page = PageConfigurationManager.getValue(PATH_PAGE_ERROR);
			return page;
		}
		} else {
			page = PageConfigurationManager.getValue(PATH_PAGE_REGISTRATION);
			request.setAttribute(LOGIN, request.getParameter(LOGIN));
			request.setAttribute(FIRST_NAME, request.getParameter(FIRST_NAME));
			request.setAttribute(LAST_NAME, request.getParameter(LAST_NAME));
			request.setAttribute(EMAIL, request.getParameter(EMAIL));
		}
		return page;
	}
	
	/**
	 * This method checks if user building finished successful
	 * @param request
	 * @return true if user building finished successful, else return false
	 */
	public boolean build(HttpServletRequest request) {
		boolean isBuilt = true;
		isBuilt &= buildLogin(request);
		isBuilt &= buildPassword(request);
		isBuilt &= buildFirstName(request);
		isBuilt &= buildLastName(request);
		isBuilt &= buildEmail(request);
		return isBuilt;
	}

	/**
	 * This method builds user's login
	 * @param request
	 * @return true if login is built successful, else return false
	 */
	private boolean buildLogin(HttpServletRequest request){
		boolean isBuilt = false;
		String login = request.getParameter(LOGIN);
		if (login.matches(MessageManager.getValue(PATTERN_LOGIN))) {
			user.setLogin(login);
			isBuilt = true;
		} else {
			request.setAttribute(PARAM_INCORRECT_LOGIN, MessageManager.getValue(MSG_INCORRECT_LOGIN));
			logger.debug(MessageManager.getValue(MSG_INCORRECT_LOGIN));
		}
		return isBuilt;
	}
	
	/**
	 * This method builds user's password
	 * @param request
	 * @return true if password is built successful, else return false
	 */
	private boolean buildPassword(HttpServletRequest request){
		boolean isBuilt = false;
		String password = request.getParameter(PASSWORD);
		if (password != null && password.matches(MessageManager.getValue(PATTERN_PASSWORD))) {
			if (password.equals(request.getParameter(CONFIRM_PASSWORD))){
				user.setPassword(password);
				isBuilt = true;
			} else {
				request.setAttribute(PARAM_NONCONFIRMED_PASSWORD, MessageManager.getValue(MSG_NONCONFIRMED_PASSWORD));
				logger.debug(MessageManager.getValue(MSG_NONCONFIRMED_PASSWORD));
			}			
		} else {
			request.setAttribute(PARAM_INCORRECT_PASSWORD, MessageManager.getValue(MSG_INCORRECT_PASSWORD));
			logger.debug(MessageManager.getValue(MSG_INCORRECT_PASSWORD));
		}
		return isBuilt;
	}
	
	/**
	 * This method builds user's first name
	 * @param request
	 * @return true if first name is built successful, else return false
	 */
	private boolean buildFirstName(HttpServletRequest request){
		boolean isBuilt = false;
		String firstName = request.getParameter(FIRST_NAME);
		if (firstName != null && firstName.matches(MessageManager.getValue(PATTERN_NAME))) {
			user.setFirstName(firstName);
			isBuilt = true;
		} else {
			request.setAttribute(PARAM_INCORRECT_FIRST_NAME, MessageManager.getValue(MSG_INCORRECT_FIRST_NAME));
			logger.debug(MessageManager.getValue(MSG_INCORRECT_FIRST_NAME));
		}
		return isBuilt;
	}
	
	/**
	 * This method builds user's last name
	 * @param request
	 * @return true if last name is built successful, else return false
	 */
	private boolean buildLastName(HttpServletRequest request){
		boolean isBuilt = false;
		String lastName = request.getParameter(LAST_NAME);
		if (lastName != null && lastName.matches(MessageManager.getValue(PATTERN_NAME))) {
			user.setLastName(lastName);
			isBuilt = true;
		} else {
			request.setAttribute(PARAM_INCORRECT_LAST_NAME, MessageManager.getValue(MSG_INCORRECT_LAST_NAME));
			logger.debug(MessageManager.getValue(MSG_INCORRECT_LAST_NAME));
		}
		return isBuilt;
	}
	
	/**
	 * This method builds user's email
	 * @param request
	 * @return true if email is built successful, else return false
	 */
	private boolean buildEmail(HttpServletRequest request){
		boolean isBuilt = false;
		String email = request.getParameter(EMAIL);
		if (email != null && email.matches(MessageManager.getValue(PATTERN_EMAIL))) {
			user.setEmail(email);
			isBuilt = true;
		} else {
			request.setAttribute(PARAM_INCORRECT_EMAIL, MessageManager.getValue(MSG_INCORRECT_EMAIL));
			logger.debug(MessageManager.getValue(MSG_INCORRECT_EMAIL));
		}
		return isBuilt;
	}
}
