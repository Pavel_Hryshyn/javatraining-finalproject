/*
 * @(#)BookingDao.java   1.3 2015/05/25
 */
package by.epam.training.finalproject.hotel.database.dao;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

import org.apache.log4j.Logger;

import by.epam.training.finalproject.hotel.database.dao.exceptions.DaoTechException;
import by.epam.training.finalproject.hotel.database.dao.interfaces.IBookingDao;
import by.epam.training.finalproject.hotel.database.dao.util.ConvertToSQLDate;
import by.epam.training.finalproject.hotel.database.dao.util.DaoCheckersForLogging;
import by.epam.training.finalproject.hotel.i18n.MessageManager;
import by.epam.training.finalproject.hotel.model.entities.Booking;
import by.epam.training.finalproject.hotel.model.entities.HotelRoomType;
import by.epam.training.finalproject.hotel.model.entities.User;

/**
 * This class implements DAO pattern and contains methods that bind the Booking entity 
 * with database
 * @version 1.3 25 May 2015
 * @author Pavel Hryshyn
 */
public class BookingDao extends AbstractDao implements IBookingDao {
	/** This object obtains logger for this class*/
	private final static Logger logger = Logger.getLogger(BookingDao.class);
	
	/** This constants store names of database column */
	private final static String BOOKING_ID = "id";
	private final static String NUMBER_OF_GUEST = "guest_number";
	private final static String START_DATE = "start_date";
	private final static String END_DATE = "end_date";
	private final static String IS_HANDLED = "is_handled";
	private final static String BOOKING_COMMENT = "booking_comment";
	private final static String ROOM_TYPE = "room_type_type";
	private final static String USER_ID = "users.id";
	private final static String LOGIN = "login";
	private final static String FIRST_NAME = "first_name";
	private final static String LAST_NAME = "last_name";
	private final static String EMAIL = "email";

	/** This constants store SQL queries for database */
	private final static String SQL_FIND_ALL_BOOKING = "sql.booking.find.all";
	private final static String SQL_FIND_ALL_UNHANDLED_BOOKING = "sql.booking.find.all.unhandled";
	private final static String SQL_FIND_BY_ID = "sql.booking.find.by.id";
	private final static String SQL_FIND_BY_USER_ID = "sql.booking.find.by.user.id";
	private final static String SQL_DELETE = "sql.booking.delete";
	private final static String SQL_ADD = "sql.booking.add";
	private final static String SQL_UPDATE = "sql.booking.update";
	
	/** This constants store exception messages */
	private static final String EXC_SQL = "dao.exc.sql";
	
	/** This constants store logger messages */
	private static final String LOGGER_MSG_BOOKING_LIST_EMPTY = "logger.message.booking.list.empty";
	private static final String LOGGER_MSG_BOOKING_FIND_ALL = "logger.message.booking.find.all";
	private static final String LOGGER_MSG_BOOKING_FIND_ALL_BY_USER = "logger.message.booking.find.all.by.user";
	private static final String LOGGER_MSG_BOOKING_FIND_ALL_UNHAND = "logger.message.booking.find.all.unhandled";
	private static final String LOGGER_MSG_BOOKING_FIND_BY_ID = "logger.message.booking.find.by.id";
	private static final String LOGGER_MSG_BOOKING_FIND_BY_ID_NOT_FOUND = "logger.message.booking.find.by.id.not.found";
	private static final String LOGGER_MSG_BOOKING_DELETE = "logger.message.booking.delete";
	private static final String LOGGER_MSG_BOOKING_NOT_DELETE = "logger.message.booking.delete.not";
	private static final String LOGGER_MSG_BOOKING_DELETE_NOT_FOUND = "logger.message.booking.delete.not.found";
	private static final String LOGGER_MSG_BOOKING_ADD = "logger.message.booking.add";
	private static final String LOGGER_MSG_BOOKING_NO_ADD = "logger.message.booking.add.not";
	private static final String LOGGER_MSG_BOOKING_UPDATE = "logger.message.booking.update";
	private static final String LOGGER_MSG_BOOKING_NO_UPDATE = "logger.message.booking.update.not";
	
	/**
	 * This method returns all bookings that stores in database
	 * @return list of all bookings from database
	 * @throws DaoTechException, if arise database access error or other database errors
	 */
	@Override
	public List<Booking> findAll() throws DaoTechException {
		connection = getConnection();
		List<Booking> bookingList = new ArrayList<Booking>();
		try {
			Statement statement = connection.createStatement();
			resultSet = statement.executeQuery(MessageManager.getValue(SQL_FIND_ALL_BOOKING));
			while(resultSet.next()){
				bookingList.add(buildBooking(resultSet));
			}
			DaoCheckersForLogging.isEmptyListChecker(bookingList, logger, LOGGER_MSG_BOOKING_LIST_EMPTY, LOGGER_MSG_BOOKING_FIND_ALL);	
		} catch (SQLException e) {
			throw new DaoTechException(MessageManager.getValue(EXC_SQL), e);
		} finally {
			close();
		}
		return bookingList;
	}
	
	/**
	 * This method returns all unhandled bookings that stores in database
	 * @return list of all unhandled bookings from database
	 * @throws DaoTechException, if arise database access error or other database errors
	 */
	@Override
	public List<Booking> findAllUnhandledBooking() throws DaoTechException {
		connection = getConnection();
		List<Booking> bookingList = new ArrayList<Booking>();
		try {
			Statement statement = connection.createStatement();
			resultSet = statement.executeQuery(MessageManager.getValue(SQL_FIND_ALL_UNHANDLED_BOOKING));
			while(resultSet.next()){
				bookingList.add(buildBooking(resultSet));
			}
			DaoCheckersForLogging.isEmptyListChecker(bookingList, logger, LOGGER_MSG_BOOKING_LIST_EMPTY, LOGGER_MSG_BOOKING_FIND_ALL_UNHAND);
		} catch (SQLException e) {
			throw new DaoTechException(MessageManager.getValue(EXC_SQL), e);
		} finally {
			close();
		}
		return bookingList;
	}
	
	/**
	 * This method returns all bookings of some user that stores in database
	 * @return list of all bookings of some from database
	 * @param id
	 * 			the param is user id and uses for searching user's booking list from database
	 * @throws DaoTechException, if arise database access error or other database errors
	 */
	@Override
	public List<Booking> findBookingByUserId(Integer id) throws DaoTechException {
		connection = getConnection();
		List<Booking> bookingList = new ArrayList<Booking>();
		try {
			PreparedStatement statement = connection.prepareStatement(MessageManager.getValue(SQL_FIND_BY_USER_ID));
			statement.setInt(1, id);;
			resultSet = statement.executeQuery();
			while (resultSet.next()){
				bookingList.add(buildBooking(resultSet));
			}
			DaoCheckersForLogging.isEmptyListChecker(bookingList, logger, LOGGER_MSG_BOOKING_LIST_EMPTY, LOGGER_MSG_BOOKING_FIND_ALL_BY_USER);
		} catch (SQLException e) {
			throw new DaoTechException(MessageManager.getValue(EXC_SQL), e);
		} finally {
			close();
		}
		return bookingList;
	}
	
	/**
	 * This method returns booking with some id that stores in database
	 * @param id
	 * 			the param uses for searching booking in database
	 * @return booking with some id from database
	 * @throws DaoTechException, if arise database access error or other database errors
	 */
	@Override
	public Booking findById(Integer id) throws DaoTechException {
		connection = getConnection();
		Booking booking = null;
		try {
			PreparedStatement statement = connection.prepareStatement(MessageManager.getValue(SQL_FIND_BY_ID));
			statement.setInt(1, id);
			resultSet = statement.executeQuery();
			if (resultSet.next()){
				booking = buildBooking(resultSet);
				logger.info(MessageManager.getValue(LOGGER_MSG_BOOKING_FIND_BY_ID) + id);
			} else {
				logger.info(MessageManager.getValue(LOGGER_MSG_BOOKING_FIND_BY_ID_NOT_FOUND) + id);
			}
		} catch (SQLException e) {
			throw new DaoTechException(MessageManager.getValue(EXC_SQL), e);
		} finally {
			close();
		}
		return booking;
	}
	
	/**
	 * This method checks that booking with some id is exist and deletes it from database
	 * @param id
	 * 			the param uses for deleting booking from database
	 * @return true if booking is deleted from database, else false
	 * @throws DaoTechException, if arise database access error or other database errors
	 */
	@Override
	public boolean delete(Integer id) throws DaoTechException {
		Booking booking = findById(id);
		boolean isDeleted = false;
		int row = 0;
		connection = getConnection();
		try {
			if (booking != null) {
			PreparedStatement statement = connection.prepareStatement(MessageManager.getValue(SQL_DELETE));
			statement.setInt(1, id);
			row = statement.executeUpdate();
			isDeleted = DaoCheckersForLogging.executeUpdateChecker(row, logger, LOGGER_MSG_BOOKING_DELETE, LOGGER_MSG_BOOKING_NOT_DELETE);
			} else {
				logger.info(id + MessageManager.getValue(LOGGER_MSG_BOOKING_DELETE_NOT_FOUND));
			}
		} catch (SQLException e) {
			throw new DaoTechException(MessageManager.getValue(EXC_SQL), e);
		} finally {
			close();
		}
		return isDeleted;
	}
	
	/**
	 * This method adds booking to database
	 * @param booking
	 * 			the param uses for adding booking to database
	 * @return true if booking is added to database, else false
	 * @throws DaoTechException, if arise database access error or other database errors
	 */
	@Override
	public boolean add(Booking booking) throws DaoTechException {
		connection = getConnection();
		boolean isAdded = false;
		int row = 0;
		try {
			PreparedStatement statement = connection.prepareStatement(MessageManager.getValue(SQL_ADD));
			statement.setInt(1, booking.getNumberOfGuest());
			statement.setDate(2, ConvertToSQLDate.convert(booking.getStartDate()));
			statement.setDate(3, ConvertToSQLDate.convert(booking.getEndDate()));
			statement.setBoolean(4, booking.isHandledBooking());
			statement.setString(5, booking.getBookingComment());
			statement.setInt(6, booking.getUser().getId());
			statement.setString(7, booking.getRoomType().toString());
			row = statement.executeUpdate();
			isAdded = DaoCheckersForLogging.executeUpdateChecker(row, logger, LOGGER_MSG_BOOKING_ADD, LOGGER_MSG_BOOKING_NO_ADD);
		} catch (SQLException e) {
			throw new DaoTechException(MessageManager.getValue(EXC_SQL), e);
		} finally {
			close();
		}
		return isAdded;
	}
	
	/**
	 * This method updates booking to database
	 * @param booking
	 * 			the param uses for updating booking to database
	 * @return true if booking is updated to database, else false
	 * @throws DaoTechException, if arise database access error or other database errors
	 */
	@Override
	public boolean update(Booking booking) throws DaoTechException {
		connection = getConnection();
		boolean isUpdated = false;
		int row = 0;
		try {
			PreparedStatement statement = connection.prepareStatement(MessageManager.getValue(SQL_UPDATE));
			statement.setInt(1, booking.getNumberOfGuest());
			statement.setDate(2, ConvertToSQLDate.convert(booking.getStartDate()));
			statement.setDate(3, ConvertToSQLDate.convert(booking.getEndDate()));
			statement.setString(4, booking.getBookingComment());
			statement.setString(5, booking.getRoomType().toString());
			statement.setBoolean(6, booking.isHandledBooking());
			statement.setInt(7, booking.getId());
			row = statement.executeUpdate();
			isUpdated = DaoCheckersForLogging.executeUpdateChecker(row, logger, LOGGER_MSG_BOOKING_UPDATE, LOGGER_MSG_BOOKING_NO_UPDATE);
		} catch (SQLException e) {
			throw new DaoTechException(MessageManager.getValue(EXC_SQL), e);
		} finally {
			close();
		}
		return isUpdated;
	}
	
	/**
	 * This method builds booking from ResultSet
	 * @param resultSet
	 * 			the param uses for returning result of query from database
	 * @return booking
	 * @throws SQLException, if arise database access error or other database errors
	 */
	private Booking buildBooking(ResultSet resultSet) throws SQLException{
		Booking booking = new Booking();
		User user = new User();
		booking.setId(resultSet.getInt(BOOKING_ID));
		booking.setNumberOfGuest(resultSet.getInt(NUMBER_OF_GUEST));
		booking.setStartDate(resultSet.getDate(START_DATE));
		booking.setEndDate(resultSet.getDate(END_DATE));
		booking.setHandledBooking(resultSet.getBoolean(IS_HANDLED));
		booking.setBookingComment(resultSet.getString(BOOKING_COMMENT));
		booking.setRoomType(HotelRoomType.valueOf(resultSet.getString(ROOM_TYPE)));
		
		user.setId(resultSet.getInt(USER_ID));
		user.setLogin(resultSet.getString(LOGIN));
		user.setFirstName(resultSet.getString(FIRST_NAME));
		user.setLastName(resultSet.getString(LAST_NAME));
		user.setEmail(resultSet.getString(EMAIL));
		booking.setUser(user);
		return booking;
	}	
}
