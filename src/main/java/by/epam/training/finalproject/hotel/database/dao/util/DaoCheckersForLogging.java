/*
 * @(#)DaoCheckersForLogging.java   1.1 2015/07/06
 */
package by.epam.training.finalproject.hotel.database.dao.util;

import java.util.List;

import org.apache.log4j.Logger;
import by.epam.training.finalproject.hotel.i18n.MessageManager;

/**
 * This class contains method for checking results of queries to database and output log message
 * @version 1.1 06 July 2015
 * @author Pavel Hryshyn
 */
public class DaoCheckersForLogging {
	
	/**
	 * This method checks result of executing the SQL statement in PreparedStatement object
	 * and return Log message
	 * @param row
	 * 			the param is the row count for SQL Data Manipulation Language (DML) statements
	 * @param logger
	 * @param trueLog
	 * 			the param is Log message if SQL statement returns the row count
	 * @param falseLog
	 * 			the param is Log message if SQL statement returns nothing
	 * @return true if row != 0, else return false
	 */
	public static boolean executeUpdateChecker(int row, Logger logger, String trueLog, String falseLog){
		boolean isChecked = false;
		if (row != 0) {
			isChecked = true;
			logger.info(MessageManager.getValue(trueLog));
		} else {
			logger.debug(MessageManager.getValue(falseLog));
		}
		return isChecked;
	}
	
	/**
	 * This method checks if the list empty and return Log message
	 * @param list
	 * 			the param is implementation java.util.List interface
	 * @param logger
	 * @param emptyListLog
	 * 			the param is Log message if the list is empty
	 * @param falseLog
	 * 			the param is Log message if the list is not empty
	 * @return true if row != 0, else return false
	 */
	public static boolean isEmptyListChecker(List list, Logger logger, String emptyListLog, String noEmptyListLog){
		boolean isEmpty = list.isEmpty();
		if (!isEmpty){
			logger.info(MessageManager.getValue(noEmptyListLog));
		} else {
			logger.info(MessageManager.getValue(emptyListLog));
		}
		return isEmpty;
	}
}
