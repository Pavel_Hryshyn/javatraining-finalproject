/*
 * @(#)CharacterEncodingFilter.java   1.0 2015/05/20
 */
package by.epam.training.finalproject.hotel.web.filter;

import java.io.IOException;

import javax.servlet.Filter;
import javax.servlet.FilterChain;
import javax.servlet.FilterConfig;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;

/**
 * This class sets request and response encoding in UTF-8
 * @version 1.0 20 May 2015
 * @author Pavel Hryshyn
 */
public class CharacterEncodingFilter implements Filter {
	/** This constants store reference to encoding in web.xml */
	private static final String ENCODING = "encoding";
	
	/** This fields store encoding parameter */
	private String code;
	
	/**
	 * This method resets encoding when disconnecting the filter 
	 */
	@Override
	public void destroy() {
		code = null;
	}

	/**
	 * This method sets encoding for request and response
	 * @param request
	 * @param response
	 * @param chain
	 * 				this is chain of filters
	 * @throws ServletException, IOException
	 */
	@Override
	public void doFilter(ServletRequest request, ServletResponse response,
			FilterChain chain) throws IOException, ServletException {
		String requestEncoding = request.getCharacterEncoding();
		if (code != null && !code.equalsIgnoreCase(requestEncoding)){
			request.setCharacterEncoding(code);
			response.setCharacterEncoding(code);
		}
		chain.doFilter(request, response);
	}
	
	/**
	 * This method sets encoding when connecting the filter 
	 * @param filterConfig
	 */
	@Override
	public void init(FilterConfig filterConfig) throws ServletException {
		code = filterConfig.getInitParameter(ENCODING);
	}
}
