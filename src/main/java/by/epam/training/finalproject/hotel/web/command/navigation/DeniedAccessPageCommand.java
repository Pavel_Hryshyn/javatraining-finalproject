/*
 * @(#)DeniedAccessPageCommand.java   1.0 2015/06/25
 */
package by.epam.training.finalproject.hotel.web.command.navigation;

import javax.servlet.http.HttpServletRequest;

import org.apache.log4j.Logger;

import by.epam.training.finalproject.hotel.i18n.MessageManager;
import by.epam.training.finalproject.hotel.web.command.Command;
import by.epam.training.finalproject.hotel.web.util.PageConfigurationManager;

/**
 * This class implements command pattern and forwards to error 403 page
 * @version 1.0 25 June 2015
 * @author Pavel Hryshyn
 */
public class DeniedAccessPageCommand extends Command {
	/** This object obtains logger for this class*/
	private static final Logger logger = Logger.getLogger(DeniedAccessPageCommand.class);
	
	/** This constants store path to jsp pages */
	private static final String PATH_ERROR_PAGE_403 = "path.page.error403";
	
	/** This constants store logger messages */
	private final static String LOGGER_MSG_ERROR_403_PAGE = "logger.message.command.page.error403";
	
	/**
	 * This method forwards to error 403 page
	 * @param request
	 * @return page where request is forwarded
	 */
	@Override
	public String execute(HttpServletRequest request) {
		String page = PageConfigurationManager.getValue(PATH_ERROR_PAGE_403);
		logger.debug(MessageManager.getValue(LOGGER_MSG_ERROR_403_PAGE));
		return page;
	}
}
