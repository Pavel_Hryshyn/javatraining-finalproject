/*
 * @(#)AccessToCommandChecker.java   1.0 2015/06/25
 */
package by.epam.training.finalproject.hotel.web.util;

import javax.servlet.http.HttpServletRequest;
import by.epam.training.finalproject.hotel.model.entities.User;
import by.epam.training.finalproject.hotel.web.command.CommandEnum;

/**
 * This class includes methods for checking access rights to command
 * @version 1.0 25 June 2015
 * @author Pavel Hryshyn
 */
public class AccessToCommandChecker {
	/** This constants store names of request parameters */
	private static final String PARAM_USER = "user";
	
	/**
	 * This method checks if user has access rights to command
	 * @param command
	 * @param request
	 * @return true if access rights is defined else return false
	 */
	public static boolean check(CommandEnum command, HttpServletRequest request) {
		boolean isChecked = false;
		User user = (User) request.getSession().getAttribute(PARAM_USER);
		switch (command.getUserRole()) {
		case ADMIN:
			if (user != null) {
				if (user.isAdmin()){
					isChecked = true;
				}
			}
			break;
		case USER:
			if (user != null) {
				isChecked = true;
			}
			break;
		case GUEST:
			if (user == null) {
				isChecked = true;
			}
			break;
		case ALL: isChecked = true;
			break;
		default:
			isChecked = false;
		}
		return isChecked;
	}
}
