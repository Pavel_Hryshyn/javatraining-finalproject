/*
 * @(#)SecurityFilter.java   1.0 2015/06/12
 */
package by.epam.training.finalproject.hotel.web.filter;

import java.io.IOException;

import javax.servlet.Filter;
import javax.servlet.FilterChain;
import javax.servlet.FilterConfig;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 * This class prohibits direct access to a jsp page
 * @version 1.0 12 June 2015
 * @author Pavel Hryshyn
 */
public class SecurityFilter implements Filter {
	/** This constants store reference to index page in web.xml */
	private final static String INDEX_PATH = "indexPath";
	
	private String indexPath;
	
	@Override
	public void destroy() {
		// TODO Auto-generated method stub

	}
	
	/**
	 * This method prohibits direct access to a jsp page
	 * @param request
	 * @param response
	 * @param chain
	 * 				this is chain of filters
	 * @throws ServletException, IOException
	 */
	@Override
	public void doFilter(ServletRequest request, ServletResponse responce,
			FilterChain chain) throws IOException, ServletException {
		HttpServletRequest httpRequest = (HttpServletRequest)request;
		HttpServletResponse httpResponse = (HttpServletResponse)responce;
		httpResponse.sendRedirect(httpRequest.getContextPath() + indexPath);
		chain.doFilter(request, responce);
	}

	/**
	 * This method sets path to index page when connecting the filter 
	 * @param filterConfig
	 */
	@Override
	public void init(FilterConfig filterConfig) throws ServletException {
		indexPath = filterConfig.getInitParameter(INDEX_PATH);
	}

}
