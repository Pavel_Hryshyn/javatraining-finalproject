/*
 * @(#)Invoice.java   1.0 2015/05/13
 */
package by.epam.training.finalproject.hotel.model.entities;

import java.io.Serializable;

import by.epam.training.finalproject.hotel.model.entities.interfaces.IEntity;

/**
 * This class is entity that defines the parameters of invoices
 * @version 1.0 13 May 2015
 * @author Pavel Hryshyn
 */
public class Invoice implements Serializable, IEntity {
	private static final long serialVersionUID = -6299687364751901043L;

	private int id;	
	private double totalAmount;
	private String invoiceComment;
	private HotelRoom hotelRoom;
	private Booking booking;
	
	public Invoice() {
		super();
		// TODO Auto-generated constructor stub
	}

	@Override
	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public double getTotalAmount() {
		return totalAmount;
	}

	public void setTotalAmount(double totalAmount) {
		this.totalAmount = totalAmount;
	}

	public String getInvoiceComment() {
		return invoiceComment;
	}

	public void setInvoiceComment(String invoiceComment) {
		this.invoiceComment = invoiceComment;
	}

	public HotelRoom getHotelRoom() {
		return hotelRoom;
	}

	public void setHotelRoom(HotelRoom hotelRoom) {
		this.hotelRoom = hotelRoom;
	}

	public Booking getBooking() {
		return booking;
	}

	public void setBooking(Booking booking) {
		this.booking = booking;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((booking == null) ? 0 : booking.hashCode());
		result = prime * result
				+ ((hotelRoom == null) ? 0 : hotelRoom.hashCode());
		result = prime * result
				+ ((invoiceComment == null) ? 0 : invoiceComment.hashCode());
		result = prime * result + id;
		long temp;
		temp = Double.doubleToLongBits(totalAmount);
		result = prime * result + (int) (temp ^ (temp >>> 32));
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Invoice other = (Invoice) obj;
		if (booking == null) {
			if (other.booking != null)
				return false;
		} else if (!booking.equals(other.booking))
			return false;
		if (hotelRoom == null) {
			if (other.hotelRoom != null)
				return false;
		} else if (!hotelRoom.equals(other.hotelRoom))
			return false;
		if (invoiceComment == null) {
			if (other.invoiceComment != null)
				return false;
		} else if (!invoiceComment.equals(other.invoiceComment))
			return false;
		if (id != other.id)
			return false;
		if (Double.doubleToLongBits(totalAmount) != Double
				.doubleToLongBits(other.totalAmount))
			return false;
		return true;
	}

	@Override
	public String toString() {
		return "Invoice: No=" + id + ", totalAmount="
				+ totalAmount + ", invoiceComment=" + invoiceComment
				+ ", hotelRoom=" + hotelRoom + ", booking=" + booking;
	}
}
