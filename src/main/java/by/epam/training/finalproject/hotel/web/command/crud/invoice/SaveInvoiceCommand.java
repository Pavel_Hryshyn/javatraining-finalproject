/*
 * @(#)SaveInvoiceCommand.java   1.4 2015/06/24
 */
package by.epam.training.finalproject.hotel.web.command.crud.invoice;

import java.text.ParseException;

import javax.servlet.http.HttpServletRequest;

import org.apache.log4j.Logger;

import by.epam.training.finalproject.hotel.database.dao.BookingDao;
import by.epam.training.finalproject.hotel.database.dao.HotelRoomDao;
import by.epam.training.finalproject.hotel.database.dao.InvoiceDao;
import by.epam.training.finalproject.hotel.database.dao.exceptions.DaoTechException;
import by.epam.training.finalproject.hotel.i18n.MessageManager;
import by.epam.training.finalproject.hotel.model.entities.Booking;
import by.epam.training.finalproject.hotel.model.entities.HotelRoom;
import by.epam.training.finalproject.hotel.model.entities.Invoice;
import by.epam.training.finalproject.hotel.web.command.Command;
import by.epam.training.finalproject.hotel.web.util.PageConfigurationManager;

/**
 * This class implements command pattern and save invoice to database
 * @version 1.5 18 June 2015
 * @author Pavel Hryshyn
 */
public class SaveInvoiceCommand extends Command {
	/** This object obtains logger for this class*/
	private final static Logger logger = Logger.getLogger(SaveInvoiceCommand.class);
	
	/** This constants store names of request parameters */
	private static final String PARAM_BOOKING_ID = "bookingId";
	private static final String PARAM_TOTAL_AMOUNT ="totalAmount";
	private static final String PARAM_INVOICE_COMMENT = "invoiceComment";
	private static final String PARAM_INCORRECT_TOTAL_AMOUNT = "incorrectTotalAmount";
	private static final String PARAM_ROOM_ID = "hotelRoomId";
	private static final String PARAM_INVOICE_CREATED = "invoiceCreated";
	private static final String PARAM_INVOICE_UPDATED = "invoiceUpdated";
	private static final String PARAM_BOOKING_LIST = "bookingList";
	private static final String PARAM_INVOICE = "invoice";
	private static final String PARAM_INVOICE_ID = "invoiceId";
	
	/** This constants store messages that output to jsp page */
	private static final String MSG_INCORRECT_TOTAL_AMOUNT = "jsp.message.incorrect.total.amount";
	private static final String MSG_INVOICE_CREATED = "jsp.message.invoice.created";
	private static final String MSG_INVOICE_UPDATED = "jsp.message.invoice.updated";
	
	/** This constants store path to jsp pages */
	private static final String PATH_PAGE_BOOKING_LIST = "path.page.booking.list";
	private static final String PATH_PAGE_ADD_INVOICE = "path.page.invoice.add";
	private static final String PATH_PAGE_ERROR = "path.page.error";
	
	/** This constants store logger messages */
	private final static String LOGGER_MSG_INVOICE_ADD = "logger.message.command.invoice.add";
	private final static String LOGGER_MSG_INVOICE_UPDATE = "logger.message.command.invoice.update";
	
	private InvoiceDao invoiceDao = new InvoiceDao();
	private BookingDao bookingDao = new BookingDao();
	private HotelRoomDao roomDao = new HotelRoomDao();
	private Booking booking;
	private Invoice invoice;

	@Override
	public String execute(HttpServletRequest request) {
		String page = null;
		try {
			if (request.getParameter(PARAM_INVOICE_ID)==null) {
				if (!addInvoice(request)){
					request.setAttribute(PARAM_INVOICE, invoice);
					page = PageConfigurationManager.getValue(PATH_PAGE_ADD_INVOICE);
				} 
			} else {
				updateInvoice(request);
			}
		} catch(ParseException e) {
			logger.debug(e);
			page = PageConfigurationManager.getValue(PATH_PAGE_ADD_INVOICE);
			return page;
		} catch(DaoTechException e) {
			logger.error(e);
			page = PageConfigurationManager.getValue(PATH_PAGE_ERROR);
			return page;
		}	
		page = PageConfigurationManager.getValue(PATH_PAGE_BOOKING_LIST);
		return page;
	}
	
	/**
	 * This method checks if building of invoice finished successful
	 * @param request
	 * @return true if building of invoice finished successful, else return false
	 */
	public boolean build(HttpServletRequest request) throws DaoTechException {
		boolean isBuilt = true;
		isBuilt &= buildTotalAmount(request);
		isBuilt &= buildHotelRoom(request);
		isBuilt &= buildInvoiceComment(request);
		isBuilt &= buildBooking(request);
		return isBuilt;
	}
	
	/**
	 * This method builds invoice's total amount
	 * @param request
	 * @return true if total amount is built successful, else return false
	 */
	private boolean buildTotalAmount(HttpServletRequest request){
		boolean isBuilt = false;
		double totalAmount = Double.parseDouble(request.getParameter(PARAM_TOTAL_AMOUNT));
		if (totalAmount > 0) {
			invoice.setTotalAmount(totalAmount);
			isBuilt = true;
		} else {
			request.setAttribute(PARAM_INCORRECT_TOTAL_AMOUNT, MessageManager.getValue(MSG_INCORRECT_TOTAL_AMOUNT));
		}
		return isBuilt;
	}
	
	/**
	 * This method builds invoice's hotel room
	 * @param request
	 * @return true if hotel room is built successful, else return false
	 */
	private boolean buildHotelRoom(HttpServletRequest request) throws DaoTechException{
		boolean isBuilt = false;
		int roomId = Integer.parseInt(request.getParameter(PARAM_ROOM_ID));
		HotelRoom room = roomDao.findById(roomId);
		if (room != null) {
			invoice.setHotelRoom(room);;
			isBuilt = true;
		}
		return isBuilt;
	}	

	/**
	 * This method builds invoice's comment
	 * @param request
	 * @return true if comment is built successful, else return false
	 */
	private boolean buildInvoiceComment(HttpServletRequest request){
		boolean isBuilt = false;
		String invoiceComment = request.getParameter(PARAM_INVOICE_COMMENT);
		invoice.setInvoiceComment(invoiceComment);
		isBuilt = true;
		return isBuilt;
	}
	
	/**
	 * This method builds invoice's booking
	 * @param request
	 * @return true if booking is built successful, else return false
	 */
	private boolean buildBooking(HttpServletRequest request) throws DaoTechException {
		boolean isBuilt = false;
		int bookingId = Integer.parseInt(request.getParameter(PARAM_BOOKING_ID));
		booking = bookingDao.findById(bookingId);
		if (booking != null) {
			booking.setHandledBooking(true);
			bookingDao.update(booking);
			invoice.setBooking(booking);
			isBuilt = true;
		}
		return isBuilt;	
	}
	
	/**
	 * This method adds invoice to database
	 * @param request
	 * @return true if invoice is added, else return false
	 * @throws DaoTechException 
	 */
	private boolean addInvoice(HttpServletRequest request) throws DaoTechException {
		boolean isAdded = false;
		invoice = new Invoice();
		if (build(request)) {
			isAdded = invoiceDao.add(invoice);
			request.setAttribute(PARAM_INVOICE_CREATED, MessageManager.getValue(MSG_INVOICE_CREATED));
			logger.info(MessageManager.getValue(LOGGER_MSG_INVOICE_ADD));
		}
		return isAdded;
	}
	
	/**
	 * This method updates invoice to database
	 * @param request
	 * @return true if invoice is updated, else return false
	 * @throws DaoTechException 
	 */
	private boolean updateInvoice(HttpServletRequest request) throws DaoTechException, ParseException{
		boolean isUpdated = false;
		int invoiceId = Integer.parseInt(request.getParameter(PARAM_INVOICE_ID));
		invoice = invoiceDao.findById(invoiceId);
		if (buildHotelRoom(request) && buildInvoiceComment(request)) {
			isUpdated = invoiceDao.update(invoice);
			request.setAttribute(PARAM_INVOICE_UPDATED, MessageManager.getValue(MSG_INVOICE_UPDATED));
			request.setAttribute(PARAM_BOOKING_LIST, bookingDao.findAll());
			logger.info(MessageManager.getValue(LOGGER_MSG_INVOICE_UPDATE));
		}
		return isUpdated;
	}
}
