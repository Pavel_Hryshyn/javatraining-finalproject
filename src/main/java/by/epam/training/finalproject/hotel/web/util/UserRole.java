/*
 * @(#)UserRole.java   1.0 2015/05/25
 */
package by.epam.training.finalproject.hotel.web.util;

/**
 * This enum stores user roles
 * @version 1.0 25 May 2015
 * @author Pavel Hryshyn
 */
public enum UserRole {
	 GUEST, ADMIN, USER, ALL;
}
