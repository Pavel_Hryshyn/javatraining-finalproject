/*
 * @(#)HotelRoom.java   1.0 2015/05/13
 */
package by.epam.training.finalproject.hotel.model.entities;

import java.io.Serializable;

import by.epam.training.finalproject.hotel.model.entities.interfaces.IEntity;

/**
 * This class is HotelRoom entity and defines parameters every room
 * @version 1.0 13 May 2015
 * @author Pavel Hryshyn
 */
public class HotelRoom implements Serializable, IEntity {
	private static final long serialVersionUID = -1505780166454122156L;
	
	
	private int id;
	private HotelRoomType roomType;
	private int capacity;
	private double price;
	private String description;
	
	public HotelRoom() {
		super();
		// TODO Auto-generated constructor stub
	}

	@Override
	public int getId() {
		return id;
	}

	public void setId(int roomNumber) {
		this.id = roomNumber;
	}

	public HotelRoomType getRoomType() {
		return roomType;
	}

	public void setRoomType(HotelRoomType roomType) {
		this.roomType = roomType;
	}

	public int getCapacity() {
		return capacity;
	}

	public void setCapacity(int capacity) {
		this.capacity = capacity;
	}

	public double getPrice() {
		return price;
	}

	public void setPrice(double price) {
		this.price = price;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + capacity;
		result = prime * result
				+ ((description == null) ? 0 : description.hashCode());
		long temp;
		temp = Double.doubleToLongBits(price);
		result = prime * result + (int) (temp ^ (temp >>> 32));
		result = prime * result + id;
		result = prime * result
				+ ((roomType == null) ? 0 : roomType.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		HotelRoom other = (HotelRoom) obj;
		if (capacity != other.capacity)
			return false;
		if (description == null) {
			if (other.description != null)
				return false;
		} else if (!description.equals(other.description))
			return false;
		if (Double.doubleToLongBits(price) != Double
				.doubleToLongBits(other.price))
			return false;
		if (id != other.id)
			return false;
		if (roomType != other.roomType)
			return false;
		return true;
	}

	@Override
	public String toString() {
		return "HotelRoom: roomNumber=" + id + ", roomType=" + roomType
				+ ", capacity=" + capacity + ", price=" + price
				+ ", description=" + description;
	}
	
	
	
	
	
}
