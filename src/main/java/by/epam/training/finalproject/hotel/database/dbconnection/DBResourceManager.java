/*
 * @(#)DBResourceManager.java   1.0 2015/05/14
 */
package by.epam.training.finalproject.hotel.database.dbconnection;

import java.util.ResourceBundle;

/**
 * This class contains methods that get parameters of database connection from file
 * @version 1.0 14 May 2015
 * @author Pavel Hryshyn
 */
public class DBResourceManager {
	/** This constants store path in resources folder to .properties file with parameters of database connection */
	private final static String DB_RESOURCE_PATH = "database";
	
	/** This field is instance of database resource manager */
	private final static DBResourceManager instance = new DBResourceManager();
	
	/** This field is a resource bundle  */
	private static ResourceBundle bundle = ResourceBundle.getBundle(DB_RESOURCE_PATH);
	
	/**
	 * This method gets instance of database resource manager
	 */
	public static DBResourceManager getInstance() {
		return instance;
	}
	
	/**
	 * This method gets a string for the given key from resource bundle.
	 */
	public String getValue(String key){
		return bundle.getString(key);
	}
}
