/*
 * @(#)UpdateBookingCommand.java   1.2 2015/06/15
 */
package by.epam.training.finalproject.hotel.web.command.crud.booking;

import javax.servlet.http.HttpServletRequest;

import org.apache.log4j.Logger;

import by.epam.training.finalproject.hotel.database.dao.BookingDao;
import by.epam.training.finalproject.hotel.database.dao.exceptions.DaoTechException;
import by.epam.training.finalproject.hotel.model.entities.Booking;
import by.epam.training.finalproject.hotel.web.command.Command;
import by.epam.training.finalproject.hotel.web.command.crud.user.UpdateUserCommand;
import by.epam.training.finalproject.hotel.web.util.PageConfigurationManager;

/**
 * This class implements command pattern and sets parameters for updating booking
 * @version 1.2 10 June 2015
 * @author Pavel Hryshyn
 */
public class UpdateBookingCommand extends Command {
	/** This object obtains logger for this class*/
	private final static Logger logger = Logger.getLogger(UpdateUserCommand.class);
	
	/** This constants store names of request parameters */
	private final static String PARAM_BOOKING = "booking";
	private final static String PARAM_BOOKING_ID = "bookingId";
	
	/** This constants store path to jsp pages */
	private final static String PATH_PAGE_ERROR = "path.page.error";
	private final static String PATH_PAGE_ADD_BOOKING = "path.page.booking.add";
		
	private BookingDao bookingDao = new BookingDao();
	private Booking booking;
	
	/**
	 * This method sets parameters for updating booking
	 * @param request
	 * @return page where request is forwarded
	 */
	@Override
	public String execute(HttpServletRequest request) {
		String page = null;
		int bookingId = Integer.parseInt(request.getParameter(PARAM_BOOKING_ID));
		try {
			booking = bookingDao.findById(bookingId);
		} catch (DaoTechException e) {
			logger.error(e);
			page = PageConfigurationManager.getValue(PATH_PAGE_ERROR);
			return page;
		}
		request.setAttribute(PARAM_BOOKING, booking);
		page = PageConfigurationManager.getValue(PATH_PAGE_ADD_BOOKING);
		return page;
	}

}
