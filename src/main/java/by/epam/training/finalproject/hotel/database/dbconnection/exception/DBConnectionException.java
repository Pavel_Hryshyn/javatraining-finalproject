/*
 * @(#)DBConnectionException.java   1.0 2015/05/14
 */
package by.epam.training.finalproject.hotel.database.dbconnection.exception;

/**
 * This class is wrapper for all connection pool level exceptions  
 * @version 1.0 14 May 2015
 * @author Pavel Hryshyn
 */
public class DBConnectionException extends Exception {
	private static final long serialVersionUID = -5327459994954162989L;

	public DBConnectionException(String message, Throwable cause) {
		super(message, cause);
		// TODO Auto-generated constructor stub
	}
	
}
