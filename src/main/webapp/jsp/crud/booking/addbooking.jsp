<!DOCTYPE html>
<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib prefix="tg" uri="tags"%>

<html lang="language">
<head>
<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="viewport" content="width=device-width, initial-scale=1">
<!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->


<!-- Bootstrap -->
<link
	href="${pageContext.request.contextPath}/bootstrap/css/bootstrap.css"
	rel="stylesheet">
<link
	href="${pageContext.request.contextPath}/bootstrap/css/bootstrap-theme.css"
	rel="stylesheet">
<!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
<!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
<!--[if lt IE 9]>
      <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
      <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->

<!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->


<title><fmt:message key="jsp.title.booking.save" /></title>

</head>
<body>
	<script
		src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.3/jquery.min.js"></script>
	<!-- Include all compiled plugins (below), or include individual files as needed -->
	<script
		src="${pageContext.request.contextPath}/bootstrap/js/bootstrap.js"></script>



	<jsp:include page="/jsp/header/header.jsp" />
	<jsp:include page="/jsp/menu/menu.jsp" />


	<form class="form-horizontal" name="bookingForm" method="POST"
		action="controller">
		<input type="hidden" name="command" value="save_booking" />
		<c:if test="${booking.id ne 0}">
			<div class="form-group">
				<label for="BookingId" class="col-md-2 col-lg-2 control-label"><fmt:message
						key="jsp.booking.id" /></label>
				<div class="col-md-4 col-lg-4">
					<input type="text" class="form-control" id="BookingId"
						name="bookingId" value="${booking.id}" readonly="readonly">
				</div>
			</div>
		</c:if>
		<div class="form-group">
			<label for="RoomType" class="col-md-2 col-lg-2 control-label"><fmt:message
					key="jsp.booking.room.type" /></label>
			<div class="col-md-4 col-lg-4">
				<select class="form-control" id="roomType" name="roomType">
					<c:if test="${not empty booking.roomType}">
						<option selected value="${booking.roomType}">${booking.roomType}</option>
					</c:if>
					<option value="STANDART"><fmt:message
							key="jsp.room.type.standart" /></option>
					<option value="BEDROOM"><fmt:message
							key="jsp.room.type.bedroom" /></option>
					<option value="SUITE"><fmt:message
							key="jsp.room.type.suite" /></option>
					<option value="BUSINESS_ROOM"><fmt:message
							key="jsp.room.type.business" /></option>
					<option value="PRESIDENT_SUITES"><fmt:message
							key="jsp.room.type.president" /></option>
				</select>
			</div>
			<c:if test="${not empty incorrectRoomType}">
				<div class="col-md-6 col-lg-6 alert alert-danger" role="alert">
					<c:out value="${incorrectRoomType}" />
				</div>
			</c:if>
		</div>
		<div class="form-group">
			<label for="numberOfGuest" class="col-md-2 col-lg-2 control-label"><fmt:message
					key="jsp.booking.number.of.guest" /></label>
			<div class="col-md-4 col-lg-4">
				<input type="text" class="form-control" id="numberOfGuest"
					name="numberOfGuest" value="${booking.numberOfGuest}">
			</div>
			<c:if test="${not empty incorrectNumberOfGuest}">
				<div class="col-md-6 col-lg-6 alert alert-danger" role="alert">
					<c:out value="${incorrectNumberOfGuest}" />
				</div>
			</c:if>
		</div>
		<div class="form-group">
			<label for="startDate" class="col-md-2 col-lg-2 control-label"><fmt:message
					key="jsp.booking.start.date" /></label>
			<div class="col-md-4 col-lg-4">
				<input type="text" class="form-control" id="startDate"
					placeholder="yyyy-MM-dd" name="startDate"
					value="<fmt:formatDate value="${booking.startDate}" pattern="yyyy-MM-dd"/>">
			</div>
			<c:if
				test="${not empty incorrectStartDate or not empty errorStartDateBefore}">
				<div class="col-md-6 col-lg-6 alert alert-danger" role="alert">
					<c:out value="${errorStartDateBefore}" />
					<c:out value="${incorrectStartDate}" />
				</div>
			</c:if>
		</div>
		<div class="form-group">
			<label for="endDate" class="col-md-2 col-lg-2 control-label"><fmt:message
					key="jsp.booking.end.date" /></label>
			<div class="col-md-4 col-lg-4">
				<input type="text" class="form-control" id="startDate"
					placeholder="yyyy-MM-dd" name="endDate"
					value="<fmt:formatDate value="${booking.endDate}" pattern="yyyy-MM-dd"/>">
			</div>
			<c:if
				test="${not empty incorrectEndDate or not empty errorEndDateBefore}">
				<div class="col-md-6 col-lg-6 alert alert-danger" role="alert">
					<c:out value="${incorrectEndDate}" />
					<c:out value="${errorEndDateBefore}" />
				</div>
			</c:if>
		</div>
		<div class="form-group">
			<label for="bookingComment" class="col-md-2 col-lg-2 control-label"><fmt:message
					key="jsp.booking.comment" /></label>
			<div class="col-md-4 col-lg-4">
				<textarea class="form-control" rows="5" name="bookingComment"><c:out
						value="${booking.bookingComment}" /></textarea>
			</div>
		</div>

		<div class="form-group">
			<div class="col-sm-offset-2 col-sm-10 col-md-10 col-lg-10">
				<input class="btn btn-primary" type="submit"
					value="<fmt:message key="jsp.button.save"/>" />
			</div>
		</div>
	</form>

	<div class="panel panel-warning">
		<div class="panel-footer"><jsp:include
				page="/jsp/header/footer.jsp" /></div>
	</div>
</body>
</html>