<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib prefix="tg" uri="tags" %>

<html lang="$language">
<head>
	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->

    <!-- Bootstrap -->
    <link href="${pageContext.request.contextPath}/bootstrap/css/bootstrap.css" rel="stylesheet">
 

    <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
      <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
      <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->
    
	<title><fmt:message key="jsp.title.booking.list" /></title>
</head>
<body>
	<!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.3/jquery.min.js"></script>
    <!-- Include all compiled plugins (below), or include individual files as needed -->
    <script src="${pageContext.request.contextPath}/bootstrap/js/bootstrap.js"></script>

	<jsp:include page="/jsp/header/header.jsp"/>
	<jsp:include page="/jsp/menu/menu.jsp"></jsp:include>
	
	<c:if test="${not empty bookingUpdated}">
		<div class="col-md-6 col-lg-6 alert alert-info" role="alert">
			<c:out value="${bookingUpdated}"/>
		</div>
	</c:if>
	<c:if test="${not empty bookingDeleted}">
		<div class="col-md-6 col-lg-6 alert alert-info" role="alert">
			<c:out value="${bookingDeleted}"/>
		</div>
	</c:if>	
	<c:if test="${not empty bookingListEmpty}">
		<div class="col-md-6 col-lg-6 alert alert-info" role="alert">
			<c:out value="${bookingListEmpty}"/>
		</div>
	</c:if>		

<div class="container-fluid" >
	<div class="row">
	<table class="table table-striped">
		<thead>
			<tr>
				<th><fmt:message key="jsp.booking.id"/></th>
				<th><fmt:message key="jsp.booking.number.of.guest"/></th>
				<th><fmt:message key="jsp.booking.room.type"/></th>
				<th><fmt:message key="jsp.booking.start.date"/></th>
				<th><fmt:message key="jsp.booking.end.date"/></th>
				<th><fmt:message key="jsp.booking.comment"/></th>
				<th><fmt:message key="jsp.booking.handled"/></th>
				<th colspan=2>Action</th>
			</tr>
		</thead>
		<tbody>
			<c:forEach items="${bookingList}" var="booking">
				<tr>
					<td><c:out value="${booking.id}"/></td>
					<td><c:out value="${booking.numberOfGuest}"/></td>
					<td><c:out value="${booking.roomType}"/></td>
					<td><fmt:formatDate pattern="yyyy-MM-dd" value="${booking.startDate}" /></td>
					<td><fmt:formatDate pattern="yyyy-MM-dd" value="${booking.endDate}" /></td>
					<td><c:out value="${booking.bookingComment}"/></td>
					<td><c:out value="${booking.handledBooking}"/></td>
					<td>
					<c:if test="${not booking.handledBooking}">
						<div class="btn-group" role="group">
						<form name="bookingUpdate" method="POST" action="controller">
							<input type="hidden" name="command" value="UPDATE_BOOKING"/>
							<input type="hidden" name="bookingId" value="${booking.id}">
							<input class="btn btn-default" type="submit" value="<fmt:message key="jsp.button.update"/>"/>
						</form>
						<form method="post" action="controller" onSubmit='return confirm("<fmt:message key="jsp.message.booking.delete.comfirm"/>")'>
							<input type="hidden" name="command" value="DELETE_BOOKING"/>
							<input type="hidden" name="bookingId" value="${booking.id}">
							<input class="btn btn-danger" type=submit value="<fmt:message key="jsp.button.delete"/>" >
						</form>
						</div>
					</c:if>
					
					<c:if test="${booking.handledBooking}">
						<form name="getInvoiceForm" method="POST" action="controller">
							<input type="hidden" name="command" value="GET_INVOICE_BY_BOOKING"/>
							<input type="hidden" name="bookingId" value="${booking.id}">
							<input class="btn btn-info" type="submit" value="<fmt:message key="jsp.button.get.invoice"/>"/>
						</form>
					</c:if>
					</td>
				</tr>
			</c:forEach>
		</tbody>
	</table>
	
	
	
	<div class="btn-group" role="group">
		<form name="getBookingList" method="POST" action="controller">
			<input type="hidden" name="command" value="BOOKING_LIST_BY_USER" />
			<input type="hidden" name="userId" value="${user.id}">
			<input class="btn btn-primary" type="submit" value="<fmt:message key="jsp.account.button.get.booking.list"/>"/>
		</form>
	</div>
	</div>	
</div>

</body>
</html>