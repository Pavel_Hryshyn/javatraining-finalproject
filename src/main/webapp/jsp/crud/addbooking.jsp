<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib prefix="tg" uri="tags" %>

<html>
<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
   <meta name="viewport" content="width=device-width, initial-scale=1">
  <title>Add booking</title> 
  <link href="/bootstrap/css/bootstrap.css" rel="stylesheet"> 
  <link href="/bootstrap/css/bootstrap-datepicker.css" rel="stylesheet">
  <script src="/bootstrap/js/jquery-1.11.3.js"></script>
  <script src="/bootstrap/js/bootstrap-datepicker.js"></script>
  <script>
  $(function() {
	  $('.datepicker').datepicker({
		    format: 'mm/dd/yyyy',
		    startDate: '-3d'
		});
  });
  </script>
</head>
<body>


<jsp:include page="/jsp/header/header.jsp"/>
<jsp:include page="/jsp/menu/menu.jsp"/>

<form class="form-horizontal" name="bookingForm" method="POST" action="controller">
  <input type="hidden" name="command" value="save_booking" />
  <c:if test="${not empty bookingId}">
  	<div class="form-group">
    	<label for="BookingId" class="col-md-2 col-lg-2 control-label"><fmt:message key="jsp.booking.id" /></label>
    	<div class="col-md-4 col-lg-4">
			<input type="text" class="form-control" id="BookingId" name="bookingId" value="${bookingId}" readonly="readonly">
 		</div>
  	</div>
  </c:if>
  	<div class="form-group">
    	<label for="RoomType" class="col-md-2 col-lg-2 control-label"><fmt:message key="jsp.booking.room.type" /></label>
    	<div class="col-md-4 col-lg-4">
			<select class="form-control" id="roomType" name="roomType">
  				<c:if test="${not empty roomType}">
  					<option selected value="${roomType}">${roomType}</option>
  				</c:if>
				<option value="STANDART"><fmt:message key="jsp.room.type.standart" /></option>
				<option value="BEDROOM"><fmt:message key="jsp.room.type.bedroom" /></option>
        		<option value="SUITE"><fmt:message key="jsp.room.type.suite" /></option>
        		<option value="BUSINESS_ROOM"><fmt:message key="jsp.room.type.business" /></option>
        		<option value="PRESIDENT_SUITES"><fmt:message key="jsp.room.type.president" /></option>
    		</select>
 		</div>
 		<c:if test="${not empty incorrectRoomType}">
			<div class="col-md-6 col-lg-6 alert alert-danger" role="alert">
				<c:out value="${incorrectRoomType}"/>
			</div>
		</c:if>	
  	</div>
  	<div class="form-group">
    	<label for="numberOfGuest" class="col-md-2 col-lg-2 control-label"><fmt:message key="jsp.booking.number.of.guest" /></label>
    	<div class="col-md-4 col-lg-4">
			<input type="text" class="form-control" id="numberOfGuest" name="numberOfGuest" value="${numberOfGuest}">
 		</div>
 		<c:if test="${not empty incorrectNumberOfGuest}">
			<div class="col-md-6 col-lg-6 alert alert-danger" role="alert">
				<c:out value="${incorrectNumberOfGuest}"/>
			</div>
		</c:if>	
  	</div>  	
  	<div class="form-group">
    	<label for="startDate" class="col-md-2 col-lg-2 control-label"><fmt:message key="jsp.booking.start.date" /></label>
    	<div class="col-md-4 col-lg-4">
			<input type="text" class="form-control" id="datepicker" placeholder="yyyy-MM-dd" name="startDate" value="<fmt:formatDate value="${startDate}" pattern="yyyy-MM-dd"/>">
 		</div>
  	<c:if test="${not empty incorrectStartDate or not empty errorStartDateBefore}">
		<div class="col-md-6 col-lg-6 alert alert-danger" role="alert">
			<c:out value="${errorStartDateBefore}"/>
			<c:out value="${incorrectStartDate}"/>
		</div>
	</c:if>	
 	</div>
 	<div class="form-group">
    	<label for="endDate" class="col-md-2 col-lg-2 control-label"><fmt:message key="jsp.booking.end.date" /></label>
    	<div class="col-md-4 col-lg-4">
			<input type="text" class="form-control" id="datepicker" placeholder="yyyy-MM-dd" name="endDate" value="<fmt:formatDate value="${endDate}" pattern="yyyy-MM-dd"/>">
 		</div>
  	<c:if test="${not empty incorrectEndDate or not empty errorEndDateBefore}">
		<div class="col-md-6 col-lg-6 alert alert-danger" role="alert">
			<c:out value="${incorrectEndDate}"/>
			<c:out value="${errorEndDateBefore}"/>
		</div>
	</c:if>	
 	</div>
  	<div class="form-group">
    	<label for="bookingComment" class="col-md-2 col-lg-2 control-label"><fmt:message key="jsp.booking.comment" /></label>
    	<div class="col-md-4 col-lg-4">
    		<textarea class="form-control" rows="5" name="bookingComment"><c:out value="${bookingComment}"/></textarea>
 		</div>
 	</div>
	
	<div class="form-group">
		<div class="col-sm-offset-2 col-sm-10 col-md-10 col-lg-10">
			<input class="btn btn-primary" type="submit" value="<fmt:message key="jsp.button.save"/>"/>
		</div>
	</div>
</form>

</body>
</html>