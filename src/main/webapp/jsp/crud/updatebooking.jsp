<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib prefix="tg" uri="tags" %>

<html>
<head>
  <meta charset="utf-8">
  <title>Add booking</title>
  <link rel="stylesheet" href="//code.jquery.com/ui/1.11.4/themes/smoothness/jquery-ui.css">
  <script src="//code.jquery.com/jquery-1.10.2.js"></script>
  <script src="//code.jquery.com/ui/1.11.4/jquery-ui.js"></script>
  <link rel="stylesheet" href="/resources/demos/style.css">
  <script>
  $(function() {
	    $( "#startDate" ).datepicker({
	      defaultDate: "+1w",
	      dateFormat: "yy-mm-dd",
	      changeMonth: true,
	      numberOfMonths: 1,
	      onClose: function( selectedDate ) {
	        $( "#endDate" ).datepicker( "option", "minDate", selectedDate );
	      }
	    });
	    $( "#endDate" ).datepicker({
	      defaultDate: "+1w",
	      dateFormat: "yy-mm-dd",
	      changeMonth: true,
	      numberOfMonths: 1,
	      onClose: function( selectedDate ) {
	        $( "#startDate" ).datepicker( "option", "maxDate", selectedDate );
	      }
	    });
	  });
  </script>
</head>
<body>
<jsp:include page="/jsp/menu/menu.jsp"></jsp:include>

<form class="form-horizontal" name="bookingForm" method="POST" action="controller">
  <c:if test="${empty booking}">
  <input type="hidden" name="command" value="add_booking" />
  </c:if>
  <c:if test="${not empty booking}">
  <input type="hidden" name="command" value="update_booking" />
  </c:if>
  <c:if test="${not empty booking}">
  	<div class="form-group">
    	<label for="BookingId" class="col-sm-2 control-label"><fmt:message key="jsp.booking.id" /></label>
    	<div class="col-sm-6 col-md-6 col-lg-6">
			<input type="text" class="form-control" id="BookingId" name="bookingId" value="${bookingId}">
 		</div>
  	</div>
  </c:if>
  <table>
  	
  	<tr>
  		<td><label for="RoomType"><fmt:message key="jsp.booking.room.type" />:</label></td>
  		<td><select id="roomType" name="roomType">
			<option value="STANDART"><fmt:message key="jsp.room.type.standart" /></option>
			<option value="BEDROOM"><fmt:message key="jsp.room.type.bedroom" /></option>
        	<option value="SUITE"><fmt:message key="jsp.room.type.suite" /></option>
        	<option value="BUSINESS_ROOM"><fmt:message key="jsp.room.type.business" /></option>
        	<option value="PRESIDENT_SUITES"><fmt:message key="jsp.room.type.president" /></option>
    		</select></td>
    	<td>${incorrectRoomType}</td>
  	</tr>
  	<tr>
  		<td><label for="numberOfGuest"><fmt:message key="jsp.booking.number.of.guest" />:</label></td>
  		<td><input type="text" name="numberOfGuest" value="${booking.numberOfGuest}"/></td>
  		<td>${incorrectNumberOfGuest}</td>
  	</tr>
  	<tr>
  		<td><label for="startDate"><fmt:message key="jsp.booking.start.date" />:</label></td>
  		<td>
  			<input type="text" id="startDate" name="startDate" value="<fmt:formatDate value="${booking.startDate}" pattern="yyyy-MM-dd"/>"/>
  		</td>
  		<td>${incorrectStartDate} ${errorStartDateBefore}</td>
  	</tr>
  	<tr>
  		<td><label for="endDate"><fmt:message key="jsp.booking.end.date" />:</label></td>
  		<td>
  			<input type="text" id="endDate" name="endDate" value="<fmt:formatDate value="${booking.endDate}" pattern="yyyy-MM-dd"/>"/>
  		</td>
  		<td>${incorrectEndDate} ${errorEndDateBefore}</td>
  	</tr>
    <tr>
  		<td><label for="bookingComment"><fmt:message key="jsp.booking.comment" />:</label></td>
  		<td><input type="text" name="bookingComment" value="${booking.bookingComment}"/></td>
  	</tr>
  </table>
<c:if test="${empty booking}">
<input type="submit" value="<fmt:message key="jsp.registration.button.registrate"/>"/>
</c:if>
<c:if test="${not empty booking}">
<input type="submit" value="<fmt:message key="jsp.booking.button.update"/>"/>
</c:if>
</form>
</body>
</html>