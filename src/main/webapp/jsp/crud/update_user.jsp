<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib prefix="tg" uri="tags" %>
This is update user page

<jsp:include page="/jsp/menu/menu.jsp"></jsp:include>
<form name="updateUserForm" method="POST" action="controller">
  <input type="hidden" name="command" value="update_user" />
  <table>
  	<tr>
  		<td><label for="Login"><fmt:message key="jsp.registration.login" />:</label></td>
  		<td><input type="text" name="login" value="${user.login}" readonly="readonly"/></td>
  	</tr>
  	<tr>
  		<td><label for="currentPassword"><fmt:message key="jsp.update.user.current.password" />:</label></td>
  		<td><input type="password" name="currentPass" value=""/></td>
  		<td>${incorrectCurrentPass}</td>
  	</tr>
  	<tr>
  		<td><label for="Password"><fmt:message key="jsp.registration.password" />:</label></td>
  		<td><input type="password" name="password" value=""/></td>
  		<td>${nonConfirmPass} ${incorrectPass}</td>
  	</tr>
  	<tr>
  		<td><label for="confirmPassword"><fmt:message key="jsp.registration.confirm.password" />:</label></td>
  		<td><input type="password" name="confirmPassword" value=""/></td>
  	</tr>
  	<tr>
  		<td><label for="firstName"><fmt:message key="jsp.registration.first.name" />:</label></td>
  		<td><input type="text" name="firstName" value="${user.firstName}"/></td>
  		<td>${incorrectFirstName}</td>
  	</tr>
  	<tr>
  		<td><label for="lastName"><fmt:message key="jsp.registration.last.name" />:</label></td>
  		<td><input type="text" name="lastName" value="${user.lastName}"/></td>
  		<td>${incorrectLastName}</td>
  	</tr>
    <tr>
  		<td><label for="Email"><fmt:message key="jsp.registration.email" />:</label></td>
  		<td><input type="text" name="email" value="${user.email}"/></td>
  		<td>${incorrectEmail}</td>
  	</tr>
  </table>
<input type="submit" value="<fmt:message key="jsp.registration.button.registrate"/>"/>
</form><hr/>

