<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<c:set var="language" value="${not empty param.language ? param.language : not empty language ? language : pageContext.request.locale}" scope="session" />
<fmt:setLocale value="${language}" />
<fmt:setBundle basename="i18n/message" />

<html lang="${language}">
<head><title>Registration success</title></head>
<body>
<jsp:include page="/jsp/menu/menu.jsp"></jsp:include>
<fmt:message key="jsp.registration.congratulation" />
<a href="jsp/auth/login.jsp"><fmt:message key="jsp.registration.sign" /></a>
</body>
</html>